/***********************************************************************
 *  File       : converter.c
 *  Description: Implementation of a three-phase AC inverter using
 *               F28335 ePWM modules
 *
 *  Created on : 18 May 2019
 *      Author : Kyle Steyn
 ***********************************************************************/

#include "converter.h"

/************************************************************************
 *  Initialize global variables
 ************************************************************************/
float32 duty_cycle_c1_k = 0.0;
float32 duty_cycle_c1_km1 = 0.0;
float32 error_signal_c1_k = 0.0;
float32 error_signal_c1_km1 = 0.0;

float32 duty_cycle_c2_k = 0.0;
float32 duty_cycle_c2_km1 = 0.0;
float32 error_signal_c2_k = 0.0;
float32 error_signal_c2_km1 = 0.0;

float32 duty_cycle_c3_k = 0.0;
float32 duty_cycle_c3_km1 = 0.0;
float32 error_signal_c3_k = 0.0;
float32 error_signal_c3_km1 = 0.0;

float32 duty_cycle_k = 0.0;
float32 duty_cycle_km1 = 0.0;
float32 error_signal_k = 0.0;
float32 error_signal_km1 = 0.0;

float32 test = 0.0;
float32 current = 0.0;
float32 iref = 0.0;
float32 voltage = 0.0;
float32 time_c = 0.0;
float32 sweep = 0.0;
float32 angle = 0.0;
float32 ampl = 0.0;
float32 vd = 0.0;

volatile Uint16 adc_meas1 = 0;
volatile Uint16 adc_meas2 = 0;
volatile Uint16 adc_meas3 = 0;
volatile Uint16 adc_meas4 = 0;
volatile Uint16 f_adc_done = 0;

volatile PowerState st_power = Start;

//New Code Decler
float32 i_L = 0.0;
float32 i_ref = 2.0;
float32 v_o = 0.0;
float32 i_o = 0.0;

/***********************************************************************
 *  Function   : isr_adc
 *  Description: Interrupt service routine triggered by ADC at
 *               end of conversion
 *
 ***********************************************************************/
__interrupt void isr_adc(void)
{
    // Store measurements
    adc_meas1 = (AdcRegs.ADCRESULT0 >> 4);      // Store A0
    adc_meas2 = (AdcRegs.ADCRESULT1 >> 4);      // Store B0
    adc_meas3 = (AdcRegs.ADCRESULT2 >> 4);      // Store A1
    adc_meas4 = (AdcRegs.ADCRESULT3 >> 4);      // Store B1

    i_L = (float32) (((5.0*adc_meas1/4095) - 2.5)/0.4);
    i_o = (float32) (((5.0*adc_meas2/4095) - 2.5)/0.4);
    v_o = (float32) (20.0*adc_meas3/4095);


    // Set flag
    f_adc_done = 1;

    // Acknowledge interrupt
    AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;           // Reset SEQ1
    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;         // Clear interrupt
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;     // Acknowledge interrupt


}

/***********************************************************************
 * Function   : isr_xint_stop
 * Description: Disable all ePWM modules
 *
 ***********************************************************************/
__interrupt void isr_xint_stop(void)
{
    st_power = Stop;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

/***********************************************************************
 * Function   : isr_xint_start
 * Description: Enable all ePWM modules
 *
 ***********************************************************************/
__interrupt void isr_xint_start(void)
{
    st_power = Start;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

/***********************************************************************
 *  Function   : init_reg_epwm
 *  Description: Initialize GPIO pin multiplexers as ePWM, set control
 *               registers and synchronize channels for three-phase arm
 *               operation
 *
 ***********************************************************************/
void init_reg_epwm(void)
{
    // Configure required GPIO multiplexer registers for ePWM
    EALLOW;
    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;         // ePWM-1A
    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 1;         // ePWM-1B
    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1;         // ePWM-2A
    GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 1;         // ePWM-2B
    GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 1;         // ePWM-3A
    GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 1;         // ePWM-3B
    EDIS;

    /************************
     * ePWM Channel 1 -> SA *
     ************************/
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_FREEZE;    // Disable timer
    EPwm1Regs.TBCTL.bit.FREE_SOFT = 0b11;       // Free Run
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;    // No clock division
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm1Regs.TBCTL.bit.PHSDIR = TB_DOWN;       // Count down after sync (ignored for up/down mode)
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;     // Disable load counter from phase register
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;      // Period register loaded from shadow register when counter = 0
    EPwm1Regs.TBCTL.bit.SYNCOSEL = 0x3;         // Disable ePWMxSYNCO signal

    EPwm1Regs.TBCTR = 0x0;                      // Clear timer counter
    EPwm1Regs.TBPRD = SW_PWM_PERIOD;            // Set timer period
    EPwm1Regs.TBPHS.half.TBPHS = SA_PWM_PHASE;  // Set timer phase

    EPwm1Regs.CMPCTL.all = 0x0;                 // Compare control registers
    EPwm1Regs.CMPA.half.CMPA = 0x0;             // Clear compare A register

    EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;        // Set output low when counter decrementing &= CMPA
    EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;          // Set output high when counter incrementing &= CMPA
    EPwm1Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;    // Do nothing when counter = period
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_CLEAR;        // Set output low when counter = zero

    EPwm1Regs.AQSFRC.all = 0x0;
    EPwm1Regs.AQCSFRC.all = 0x0;

    EPwm1Regs.DBCTL.bit.IN_MODE = 0b00;         // ePWM1-A Source for both rising and falling
    EPwm1Regs.DBCTL.bit.POLSEL = 0b10;          // Active high complementary mode
    EPwm1Regs.DBCTL.bit.OUT_MODE = 0b11;        // Dead-band for rising and falling edge
    EPwm1Regs.DBFED = DB_FED;                   // Falling edge dead-time delay
    EPwm1Regs.DBRED = DB_RED;                   // Rising edge dead-time delay

    EPwm1Regs.PCCTL.bit.CHPEN = 0x0;            // PWM chopper disabled

    EPwm1Regs.TZCTL.bit.TZA = 0b11;             // Disable current trip
    EPwm1Regs.TZCTL.bit.TZB = 0b11;

    EPwm1Regs.TBCTL.bit.CTRMODE = 0x2;          // Enable in count up/down mode

    /************************
     * ePWM Channel 2 -> SB *
     ************************/
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_FREEZE;
    EPwm2Regs.TBCTL.bit.FREE_SOFT = 0b11;
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm2Regs.TBCTL.bit.PHSDIR = TB_DOWN;
    EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;
    EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

    EPwm2Regs.TBCTR = 0x0;
    EPwm2Regs.TBPRD = SW_PWM_PERIOD;
    EPwm2Regs.TBPHS.half.TBPHS = SB_PWM_PHASE;

    EPwm2Regs.CMPCTL.all = 0x0;
    EPwm2Regs.CMPA.half.CMPA = 0x0;

    EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
    EPwm2Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.ZRO = AQ_CLEAR;

    EPwm2Regs.AQSFRC.all = 0x0;
    EPwm2Regs.AQCSFRC.all = 0x0;

    EPwm2Regs.DBCTL.bit.IN_MODE = 0b00;
    EPwm2Regs.DBCTL.bit.POLSEL = 0b10;
    EPwm2Regs.DBCTL.bit.OUT_MODE = 0b11;
    EPwm2Regs.DBFED = DB_FED;
    EPwm2Regs.DBRED = DB_RED;

    EPwm2Regs.PCCTL.bit.CHPEN = 0x0;

    EPwm2Regs.TZCTL.bit.TZA = 0b11;
    EPwm2Regs.TZCTL.bit.TZB = 0b11;

    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;

    /************************
     * ePWM Channel 3 -> SC *
     ************************/
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_FREEZE;
    EPwm3Regs.TBCTL.bit.FREE_SOFT = 0b11;
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm3Regs.TBCTL.bit.PHSDIR = TB_DOWN;
    EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;
    EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

    EPwm3Regs.TBCTR = 0x0;
    EPwm3Regs.TBPRD = SW_PWM_PERIOD;
    EPwm3Regs.TBPHS.half.TBPHS = SC_PWM_PHASE;

    EPwm3Regs.CMPCTL.all = 0x0;
    EPwm3Regs.CMPA.half.CMPA = 0x0;

    EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;
    EPwm3Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.ZRO = AQ_CLEAR;

    EPwm3Regs.AQSFRC.all = 0x0;
    EPwm3Regs.AQCSFRC.all = 0x0;

    EPwm3Regs.DBCTL.bit.IN_MODE = 0b00;
    EPwm3Regs.DBCTL.bit.POLSEL = 0b10;
    EPwm3Regs.DBCTL.bit.OUT_MODE = 0b11;
    EPwm3Regs.DBFED = DB_FED;
    EPwm3Regs.DBRED = DB_RED;

    EPwm3Regs.PCCTL.bit.CHPEN = 0x0;

    EPwm3Regs.TZCTL.bit.TZA = 0b11;
    EPwm3Regs.TZCTL.bit.TZB = 0b11;

    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
}

/***********************************************************************
 *  Function   : init_reg_adc
 *  Description: Initialize ADC for operation every ePWM period with
 *               EOC interrupt
 *
 ***********************************************************************/
void init_reg_adc(void)
{
    AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0b0011;  // Four conversions

    AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0b0000;   // Set ADCIN-A0 as 1st SEQ1 conversion
    AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 0b1000;   // Set ADCIN-B0 as 2nd SEQ1 conversion
    AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 0b0001;   // Set ADCIN-A1 as 3rd SEQ1 conversion
    AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 0b1001;   // Set ADCIN-B1 as 4th SEQ1 conversion

    AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;     // Sample on SOCA
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;       // Interrupt when conversion done

    EPwm1Regs.ETSEL.bit.SOCAEN = 1;             // Enable SOCA
    EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;   // Pulse when TBCTR = PERIOD
    EPwm1Regs.ETPS.bit.SOCAPRD = ET_1ST;        // Pulse on first event
}

/***********************************************************************
 * Function   : loop_start
 * Description: Loop executes control during runtime
 *
 ***********************************************************************/
void loop_start(void)
{
    if(f_adc_done)      // FLAG GETS SET IN ADC ISR AFTER EOC
    	GpioDataRegs.GPASET.bit.GPIO12 = 1;
    		GpioDataRegs.GPASET.bit.GPIO13 = 1;
    {
        if(FB_ENABLE)   // CLOSED LOOP NEGATIVE FEEDBACK PI CONTROL
        {
            // Copy current states to (k-1)
            error_signal_km1 = error_signal_k;
            duty_cycle_km1 = duty_cycle_k;

            // Calculate new states (k)


//            duty_cycle_k = duty_cycle_k
//                    + ((GAIN_P + (GAIN_I * SAMPLE_HALF)) * error_signal_k)
//                    + (((GAIN_I * SAMPLE_HALF) - GAIN_P) * error_signal_km1);


            // Wind-up protection
            if(duty_cycle_k > 1.0)
            {
                duty_cycle_k = 1;
            }
            else if(duty_cycle_k < 0.0)
            {
                duty_cycle_k = 0;
            }

            // Example on channel 1
            set_duty_c1(duty_cycle_k);

            //***************************************************************
            // inverter code copy into open loop when needed
            current = (adc_meas2*0.0007324-1.5); //1.0/4096;

                    	voltage = adc_meas1*0.000244; //1.0/4096;
                    	vd = adc_meas3*0.16911;
                    	//test = (iref-current)*0.5+voltage/15;


                    	if(time_c >= 1.0)
                    	        	{
                    	        		time_c = 0;
                    	        	}
                    	        	else if(time_c < 1.0)
                    	        	{
                    	        		time_c += 0.0001f;

                    	        	}
                    	        	if(sweep <1)
                    	        	{
                    	    		sweep = sweep + 0.0001f;
                    	    		angle = sweep * 314.2/2*time_c;
                    	        	}

                    	        	else if(sweep >=1)
                    	        	{
                    	        		sweep = 1;
                    	        		angle = sweep * 314.2*time_c;
                    	        	}


                    	        	ampl = 2.0f;
                    	        	duty_cycle_c1_k = (voltage-0.5f)*ampl + 0.5f;
                    	        	duty_cycle_c2_k = 1-voltage;  //only use channel 1

                    	        	if(vd > 100)
                    	        	{
                    	        		st_power = Stop;
                    	        	}

                        set_duty_c1(duty_cycle_c1_k);
                        set_duty_c2(duty_cycle_c2_k);
                        set_duty_c3(0.5f);


            //***************************************************************
            // Dc-dc code copy into open loop when needed

            //****************************************************************

        }


        //work here JB

        else    // OPEN LOOP CONTROL MODE
        {
            // USER OPEN LOOP CODE HERE
//        	iref = adc_meas3*0.000244; //1.0/4096;
//        	current = (adc_meas2*0.0007324-1.5)*2;
//        	voltage = adc_meas1*0.0007324*20;
//        	test = 0.5f; //(iref-current)*0.5; //+voltage/15;

        	i_ref = i_o + (10.0 - v_o)*1.0;
            duty_cycle_k = (i_ref - i_L)*0.1 + (v_o/30);
        	set_duty_c1(duty_cycle_k);

//            set_duty_c1(0.5f);
//        	set_duty_c2(0.5f);
//        	set_duty_c3(0.5f);

        }
        f_adc_done = 0;
    }
}

/***********************************************************************
 * Function   : loop_stop
 * Description: Loop executes while converter is in stop mode
 *
 ***********************************************************************/
void loop_stop(void)
{
	GpioDataRegs.GPACLEAR.bit.GPIO12 = 1;
	GpioDataRegs.GPACLEAR.bit.GPIO13 = 1;
	//set_duty_c1(0.0f);
    //set_duty_c2(0.0f);
    //set_duty_c3(0.0f);
}

/***********************************************************************
 * Function   : set_duty_c1
 * Description: Set channel 1 compare and action registers (duty cycle)
 * Arguments  : duty - duty cycle in decimal 0.0 -> 1.0
 *
 ***********************************************************************/
void set_duty_c1(float32 duty)
{
    // Saturate and set ePWM compare registers
    if(duty > 1.0)
    {
        duty_cycle_c1_k = 1.0;
        EPwm1Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
        EPwm1Regs.CMPA.half.CMPA = 0;
    }
    else if(duty < 0.0)
    {
        duty_cycle_c1_k = 0.0;
        EPwm1Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm1Regs.CMPA.half.CMPA = SW_PWM_PERIOD;
    }
    else // 0.0 < D < 1.0
    {
        duty_cycle_c1_k = duty;
        EPwm1Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm1Regs.CMPA.half.CMPA = (Uint16) ((1.0f - duty) * SW_PWM_PERIOD);
    }
}

/***********************************************************************
 * Function   : set_duty_c2
 * Description: Set channel 2 compare and action registers (duty cycle)
 * Arguments  : duty - duty cycle in decimal 0.0 -> 1.0
 *
 ***********************************************************************/
void set_duty_c2(float32 duty)
{
    // Saturate and set ePWM compare registers
    if(duty > 1.0)
    {
        duty_cycle_c2_k = 1.0;
        EPwm2Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
        EPwm2Regs.CMPA.half.CMPA = 0;
    }
    else if(duty < 0.0)
    {
        duty_cycle_c2_k = 0.0;
        EPwm2Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm2Regs.CMPA.half.CMPA = SW_PWM_PERIOD;
    }
    else // 0.0 < D < 1.0
    {
        duty_cycle_c2_k = duty;
        EPwm2Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm2Regs.CMPA.half.CMPA = (Uint16) ((1.0f - duty) * SW_PWM_PERIOD);
    }
}

/***********************************************************************
 * Function   : set_duty_c3
 * Description: Set channel 3 compare and action registers (duty cycle)
 * Arguments  : duty - duty cycle in decimal 0.0 -> 1.0
 *
 ***********************************************************************/
void set_duty_c3(float32 duty)
{
    // Saturate and set ePWM compare registers
    if(duty > 1.0)
    {
        duty_cycle_c3_k = 1.0;
        EPwm3Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
        EPwm3Regs.CMPA.half.CMPA = 0;
    }
    else if(duty < 0.0)
    {
        duty_cycle_c3_k = 0.0;
        EPwm3Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm3Regs.CMPA.half.CMPA = SW_PWM_PERIOD;
    }
    else // 0.0 < D < 1.0
    {
        duty_cycle_c3_k = duty;
        EPwm3Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
        EPwm3Regs.CMPA.half.CMPA = (Uint16) ((1.0f - duty) * SW_PWM_PERIOD);
    }
}

/********************************EOF************************************/
