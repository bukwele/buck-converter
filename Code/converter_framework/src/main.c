/***********************************************************************
 *  File       : main.c
 *  Description: Main source file
 *
 *  Created on : 18 May 2019
 *      Author : Kyle Steyn
 ***********************************************************************/

#include "main.h"

#include "converter.h"

/***********************************************************************
 * Function: main
 ***********************************************************************/
int main(void)
{

    // Initialize controller peripherals
    init_mcu();


    // Main loop
    for(;;)
    {
        switch(st_power)
        {
            case Start: loop_start(); break;
            case Stop: loop_stop(); break;
            case Trip: break;
            default: loop_stop(); break;
        }
    }
}

/***********************************************************************
 * Function   : init_mcu
 * Description: Initialize MCU clocks and peripherals
 *
 ***********************************************************************/
void init_mcu(void)
{
#ifdef _FLASH
    // Copy critical functions and FLASH setup to RAM
    memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (Uint32) &RamfuncsLoadSize);
    InitSysCtrl();
    InitFlash();
#else
    InitSysCtrl();
#endif

EALLOW;
SysCtrlRegs.HISPCP.all = 0x3;  //ADCCLK clock frequency (25 MHz)
EDIS;

    InitGpio();
    EALLOW;
    GpioCtrlRegs.GPADIR.bit.GPIO12 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO13 = 1;
    EDIS;


    // Disable global interrupts
    DINT;

    InitPieCtrl();

    // Clear interrupt flags
    IER = 0x0000;
    IFR = 0x0000;

    InitPieVectTable();

    // Link ISR function to PIE VECTOR TABLE
    EALLOW;
    PieVectTable.ADCINT = &isr_adc;
    PieVectTable.XINT1 = &isr_xint_start;
    PieVectTable.XINT2 = &isr_xint_stop;
    EDIS;

    InitAdc();

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = TB_DISABLE;
    EDIS;

    // CALL USER INIT FUNCTIONS HERE
    init_reg_epwm();
    init_reg_adc();

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = TB_ENABLE;
    EDIS;

    // Enable GROUP1 interrupts
    IER |= M_INT1;

    // Enable PIEADC interrupt
    PieCtrlRegs.PIEIER1.bit.INTx6 = 1;

    // Enable PIE external interrupt
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;
    PieCtrlRegs.PIEIER1.bit.INTx5 = 1;

    // Enable global interrupts
    EINT;

    // GPIO Configuration
    EALLOW;
    GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 0;        // Start button GPIO 14
    GpioCtrlRegs.GPADIR.bit.GPIO14 = 0;         // Configure as input
    GpioCtrlRegs.GPAQSEL1.bit.GPIO14 = 1;       // 6 samples
    GpioCtrlRegs.GPACTRL.bit.QUALPRD1 = 0xFF;   // Sampling window of (510/150e6) for noise
    GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = 14;  // XINT1 on GPIO14

    GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 0;        // Start button GPIO 15
    GpioCtrlRegs.GPADIR.bit.GPIO15 = 0;
    GpioCtrlRegs.GPAQSEL1.bit.GPIO15 = 1;
    GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = 15;  // XINT2 is GPIO15
    EDIS;

    // Enable (XINT1 - Start) and (XINT2 - Stop) on falling edge
    XIntruptRegs.XINT1CR.bit.POLARITY = 0;
    XIntruptRegs.XINT1CR.bit.ENABLE = 1;
    XIntruptRegs.XINT2CR.bit.POLARITY = 0;
    XIntruptRegs.XINT2CR.bit.ENABLE = 1;

    // Enable emulator real-time memory access in DEBUG mode
#ifdef _DEBUG
    ERTM;
#else
    DRTM;
#endif
}

/********************************EOF************************************/
