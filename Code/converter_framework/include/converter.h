/***********************************************************************
 *  File       : converter.h
 *  Description: Converter header file
 *
 *  Created on : 18 May 2019
 *      Author : Kyle Steyn
 ***********************************************************************/

#ifndef CONVERTER_H_
#define CONVERTER_H_

#include "DSP28x_Project.h"

/***********************************************************************
 *  Structures
 ***********************************************************************/
typedef enum  // Define state types
{
    Start,    // Input voltage positive, inductor current positive
    Stop,     // Input voltage positive, inductor current negative
    Trip      // Input voltage negative, inductor current positive
} PowerState;

/***********************************************************************
 *  Function prototypes
 ***********************************************************************/
#ifdef _FLASH
#pragma CODE_SECTION(InitFlash, "ramfuncs");
#pragma CODE_SECTION(isr_adc, "ramfuncs");
#pragma CODE_SECTION(isr_xint_stop, "ramfuncs");
#pragma CODE_SECTION(isr_xint_start, "ramfuncs");
#pragma CODE_SECTION(loop_start, "ramfuncs");
#pragma CODE_SECTION(loop_stop, "ramfuncs");
#endif

__interrupt void isr_adc(void);
__interrupt void isr_xint_stop(void);
__interrupt void isr_xint_start(void);

void init_reg_epwm(void);
void init_reg_adc(void);

void loop_start(void);
void loop_stop(void);

void set_duty_c1(float32 duty);
void set_duty_c2(float32 duty);
void set_duty_c3(float32 duty);

/***********************************************************************
 *  Definitions
 ***********************************************************************/
#define SW_PWM_PERIOD    3750u     // PWM switching frequency: 20 kHz (150e6/20000/2)

#define SA_PWM_PHASE     0u        // Arm A PWM phase delay
#define SB_PWM_PHASE     0u        // Arm B PWM phase delay
#define SC_PWM_PHASE     0u        // Arm C PWM phase delay

#define DB_FED           20u        // Falling edge dead-time
#define DB_RED           20u        // Rising edge dead-time

#define GAIN_P           0.0f      // Controller proportional gain
#define GAIN_I           0.0f      // Controller integral gain
#define SAMPLE_HALF      0.0f      // Controller half sample time
#define FB_ENABLE        0u        // Feedback loop enable

/***********************************************************************
 *  Global variable declarations
 ***********************************************************************/
// Channel 1
extern float32 duty_cycle_c1_k;      // Current (k) PWM duty cycle
extern float32 duty_cycle_c1_km1;    // Previous (k-1) PWM duty cycle
extern float32 error_signal_c1_k;    // Current control error signal (k)
extern float32 error_signal_c1_km1;  // Previous control error signal (k-1)

// Channel 2
extern float32 duty_cycle_c2_k;      // Current (k) PWM duty cycle
extern float32 duty_cycle_c2_km1;    // Previous (k-1) PWM duty cycle
extern float32 error_signal_c2_k;    // Current control error signal (k)
extern float32 error_signal_c2_km1;  // Previous control error signal (k-1)

// Channel 3
extern float32 duty_cycle_c3_k;      // Current (k) PWM duty cycle
extern float32 duty_cycle_c3_km1;    // Previous (k-1) PWM duty cycle
extern float32 error_signal_c3_k;    // Current control error signal (k)
extern float32 error_signal_c3_km1;  // Previous control error signal (k-1)

// General control system states
extern float32 duty_cycle_k;         // Current (k) PWM duty cycle
extern float32 duty_cycle_km1;       // Previous (k-1) PWM duty cycle
extern float32 error_signal_k;       // Current control error signal (k)
extern float32 error_signal_km1;     // Previous control error signal (k-1)

// ADC
extern volatile Uint16 adc_meas1;    // Store measurement from ADC-A0
extern volatile Uint16 adc_meas2;    // Store measurement from ADC-B0
extern volatile Uint16 f_adc_done;   // Flag set after measurements stored

// States
extern volatile PowerState st_power;


#endif /* CONVERTER_H_ */

/********************************EOF************************************/
