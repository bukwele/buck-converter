EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "DC-DC Buck Converter"
Date ""
Rev "1"
Comp "Stellenbosch University Thesis Projetct"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BuckConverter-rescue:NTHL020N090SC1-MyBuck Q1
U 1 1 5F4D9C49
P 8850 5250
F 0 "Q1" H 9055 5296 50  0000 L CNN
F 1 "NTHL020N090SC1" H 9055 5205 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 8550 4750 50  0001 C CIN
F 3 "https://www.onsemi.com/pub/Collateral/NTHL020N090SC1-D.PDF" H 8850 5250 50  0001 L CNN
	1    8850 5250
	1    0    0    -1  
$EndComp
$Comp
L BuckConverter-rescue:NTHL020N090SC1-MyBuck Q2
U 1 1 5F4DA34B
P 8850 6300
F 0 "Q2" H 9055 6346 50  0000 L CNN
F 1 "NTHL020N090SC1" H 9055 6255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 8550 5800 50  0001 C CIN
F 3 "https://www.onsemi.com/pub/Collateral/NTHL020N090SC1-D.PDF" H 8850 6300 50  0001 L CNN
	1    8850 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 6100 8950 5800
$Comp
L Device:L_Core_Iron L1
U 1 1 5F4DBE94
P 9900 5800
F 0 "L1" V 10125 5800 50  0000 C CNN
F 1 "L_Core_Iron" V 10034 5800 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L24.0mm_D7.5mm_P27.94mm_Horizontal_Fastron_MESC" H 9900 5800 50  0001 C CNN
F 3 "~" H 9900 5800 50  0001 C CNN
	1    9900 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 5800 8950 5800
Connection ~ 8950 5800
Wire Wire Line
	8950 5800 8950 5450
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5F544864
P 9050 2400
F 0 "J2" H 9130 2392 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 9130 2301 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 9050 2400 50  0001 C CNN
F 3 "~" H 9050 2400 50  0001 C CNN
	1    9050 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 6500 8950 6650
$Comp
L power:GND #PWR030
U 1 1 5F60478D
P 8850 2650
F 0 "#PWR030" H 8850 2400 50  0001 C CNN
F 1 "GND" H 8855 2477 50  0000 C CNN
F 2 "" H 8850 2650 50  0001 C CNN
F 3 "" H 8850 2650 50  0001 C CNN
	1    8850 2650
	1    0    0    -1  
$EndComp
Connection ~ 8950 6650
Wire Wire Line
	8950 6650 8950 6850
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5F608AD3
P 12450 6300
F 0 "J3" H 12530 6292 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 12530 6201 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 12450 6300 50  0001 C CNN
F 3 "~" H 12450 6300 50  0001 C CNN
	1    12450 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	12250 6650 12250 6400
Wire Wire Line
	12250 5800 12250 5900
Wire Wire Line
	10050 5800 10200 5800
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5F636876
P 3800 8500
F 0 "J1" H 3880 8492 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 3880 8401 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3800 8500 50  0001 C CNN
F 3 "~" H 3800 8500 50  0001 C CNN
	1    3800 8500
	-1   0    0    1   
$EndComp
$Comp
L Device:EMI_Filter_CommonMode FL1
U 1 1 5F637E5B
P 3550 3150
F 0 "FL1" H 3550 3431 50  0000 C CNN
F 1 "EMI_Filter_CommonMode" H 3550 3340 50  0000 C CNN
F 2 "BuckConverter:FIL_ACM4520-142-2P-T000" H 3550 3190 50  0001 C CNN
F 3 "~" H 3550 3190 50  0001 C CNN
	1    3550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C CBypass1
U 1 1 5F4FD622
P 12150 4700
F 0 "CBypass1" H 12265 4746 50  0000 L CNN
F 1 "0.1uF" H 12265 4655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 12188 4550 50  0001 C CNN
F 3 "~" H 12150 4700 50  0001 C CNN
	1    12150 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	11950 4850 12150 4850
$Comp
L power:GND #PWR040
U 1 1 5F4FE4A8
P 12150 4550
F 0 "#PWR040" H 12150 4300 50  0001 C CNN
F 1 "GND" H 12155 4377 50  0000 C CNN
F 2 "" H 12150 4550 50  0001 C CNN
F 3 "" H 12150 4550 50  0001 C CNN
	1    12150 4550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US RF1
U 1 1 5F4FFEFD
P 12250 5050
F 0 "RF1" V 12045 5050 50  0000 C CNN
F 1 "68" V 12136 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12290 5040 50  0001 C CNN
F 3 "~" H 12250 5050 50  0001 C CNN
	1    12250 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	11950 5050 12100 5050
Wire Wire Line
	12400 5050 12500 5050
$Comp
L Device:C CF5
U 1 1 5F503C32
P 12500 5250
F 0 "CF5" H 12615 5296 50  0000 L CNN
F 1 "100n" H 12615 5205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 12538 5100 50  0001 C CNN
F 3 "~" H 12500 5250 50  0001 C CNN
	1    12500 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	12500 5100 12500 5050
Wire Wire Line
	12500 5400 12500 5450
$Comp
L dk_PMIC-Voltage-Regulators-Linear:LM7805CT_Obsolete U5
U 1 1 5F536A5B
P 5250 8400
F 0 "U5" H 5250 8687 60  0000 C CNN
F 1 "LM7805CT_Obsolete" H 5250 8581 60  0000 C CNN
F 2 "digikey-footprints:TO-220-3" H 5450 8600 60  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/ON%20Semiconductor%20PDFs/MC7800(A,AE),NCV7800.pdf" H 5450 8700 60  0001 L CNN
F 4 "LM7805CT-ND" H 5450 8800 60  0001 L CNN "Digi-Key_PN"
F 5 "LM7805CT" H 5450 8900 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5450 9000 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 5450 9100 60  0001 L CNN "Family"
F 8 "https://media.digikey.com/pdf/Data%20Sheets/ON%20Semiconductor%20PDFs/MC7800(A,AE),NCV7800.pdf" H 5450 9200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/LM7805CT/LM7805CT-ND/458698" H 5450 9300 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 5V 1A TO220AB" H 5450 9400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 5450 9500 60  0001 L CNN "Manufacturer"
F 12 "Obsolete NonStock" H 5450 9600 60  0001 L CNN "Status"
	1    5250 8400
	1    0    0    -1  
$EndComp
$Comp
L inv_1p_cc-rescue:LM358AD-mycomp U9
U 1 1 5F53AC86
P 10850 2800
F 0 "U9" H 11400 3065 50  0000 C CNN
F 1 "LM358AD-mycomp" H 11400 2974 50  0000 C CNN
F 2 "BuckConverter:SOIC127P600X175-8N" H 11800 2900 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358a.pdf" H 11800 2800 50  0001 L CNN
F 4 "LM358ADG4, Dual Operational Amplifier 0.7MHz 5 to 28V, 8-Pin SOIC" H 11800 2700 50  0001 L CNN "Description"
F 5 "1.75" H 11800 2600 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 11800 2500 50  0001 L CNN "Manufacturer_Name"
F 7 "LM358AD" H 11800 2400 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-LM358AD" H 11800 2300 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-LM358AD" H 11800 2200 50  0001 L CNN "Mouser Price/Stock"
F 10 "6610546P" H 11800 2100 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/6610546P" H 11800 2000 50  0001 L CNN "RS Price/Stock"
	1    10850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 8950 5250 8700
Connection ~ 5250 8950
$Comp
L power:GND #PWR011
U 1 1 5F53D56D
P 5250 9050
F 0 "#PWR011" H 5250 8800 50  0001 C CNN
F 1 "GND" H 5255 8877 50  0000 C CNN
F 2 "" H 5250 9050 50  0001 C CNN
F 3 "" H 5250 9050 50  0001 C CNN
	1    5250 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 8500 4000 8950
Wire Wire Line
	4000 8950 4650 8950
$Comp
L power:+5V #PWR021
U 1 1 5F506BEA
P 5900 8150
F 0 "#PWR021" H 5900 8000 50  0001 C CNN
F 1 "+5V" H 5915 8323 50  0000 C CNN
F 2 "" H 5900 8150 50  0001 C CNN
F 3 "" H 5900 8150 50  0001 C CNN
	1    5900 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 8150 5900 8250
Wire Wire Line
	5900 8400 5700 8400
Wire Wire Line
	4550 8150 4550 8400
Connection ~ 4550 8400
Wire Wire Line
	4550 8400 4650 8400
$Comp
L power:GND #PWR032
U 1 1 5F60409A
P 8950 6850
F 0 "#PWR032" H 8950 6600 50  0001 C CNN
F 1 "GND" H 8955 6677 50  0000 C CNN
F 2 "" H 8950 6850 50  0001 C CNN
F 3 "" H 8950 6850 50  0001 C CNN
	1    8950 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5F50E978
P 3350 3300
F 0 "#PWR02" H 3350 3050 50  0001 C CNN
F 1 "GND" H 3355 3127 50  0000 C CNN
F 2 "" H 3350 3300 50  0001 C CNN
F 3 "" H 3350 3300 50  0001 C CNN
	1    3350 3300
	1    0    0    -1  
$EndComp
$Comp
L dk_PMIC-Gate-Drivers:IXDN609SI U7
U 1 1 5F52185C
P 6600 3950
F 0 "U7" H 6256 4003 60  0000 R CNN
F 1 "IXDN609SI" H 6256 3897 60  0000 R CNN
F 2 "digikey-footprints:SOIC-8-1EP_W3.9mm" H 6800 4150 60  0001 L CNN
F 3 "http://www.ixysic.com/home/pdfs.nsf/www/IXD_609.pdf/$file/IXD_609.pdf" H 6800 4250 60  0001 L CNN
F 4 "CLA364-ND" H 6800 4350 60  0001 L CNN "Digi-Key_PN"
F 5 "IXDN609SI" H 6800 4450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6800 4550 60  0001 L CNN "Category"
F 7 "PMIC - Gate Drivers" H 6800 4650 60  0001 L CNN "Family"
F 8 "http://www.ixysic.com/home/pdfs.nsf/www/IXD_609.pdf/$file/IXD_609.pdf" H 6800 4750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ixys-integrated-circuits-division/IXDN609SI/CLA364-ND/2428208" H 6800 4850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC GATE DVR 9A NON-INV 8-SOIC" H 6800 4950 60  0001 L CNN "Description"
F 11 "IXYS Integrated Circuits Division" H 6800 5050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6800 5150 60  0001 L CNN "Status"
	1    6600 3950
	1    0    0    -1  
$EndComp
$Comp
L eec:Si8261BCD-C-IS U1
U 1 1 5F526ED3
P 4100 3850
F 0 "U1" H 4700 4115 50  0000 C CNN
F 1 "Si8261BCD-C-IS" H 4700 4024 50  0000 C CNN
F 2 "BuckConverter:SOIC127P1150X265-6N" H 4100 4250 50  0001 L CNN
F 3 "http://www.silabs.com/Support%20Documents/TechnicalDocs/Si826x.pdf" H 4100 4350 50  0001 L CNN
F 4 "true" H 4100 4450 50  0001 L CNN "10kV surge"
F 5 "Manufacturer URL" H 4100 4550 50  0001 L CNN "Component Link 1 Description"
F 6 "http://www.silabs.com/" H 4100 4650 50  0001 L CNN "Component Link 1 URL"
F 7 "Package Specification" H 4100 4750 50  0001 L CNN "Component Link 3 Description"
F 8 "http://www.silabs.com/Support%20Documents/TechnicalDocs/Si826x.pdf" H 4100 4850 50  0001 L CNN "Component Link 3 URL"
F 9 "Rev. 0.9" H 4100 4950 50  0001 L CNN "Datasheet Version"
F 10 "5 to 30 V" H 4100 5050 50  0001 L CNN "Driver Supply"
F 11 "5" H 4100 5150 50  0001 L CNN "Isolation Rating Input Output kVrms"
F 12 "60" H 4100 5250 50  0001 L CNN "Max Prop Delay ns"
F 13 "Surface Mount" H 4100 5350 50  0001 L CNN "Mounting Technology"
F 14 "false" H 4100 5450 50  0001 L CNN "Overlap Protection  Dead Time Control"
F 15 "6-Pin SDIP, Body 4.58 x 7.5 mm, Pitch 1.27 mm" H 4100 5550 50  0001 L CNN "Package Description"
F 16 "ISOdrivers" H 4100 5650 50  0001 L CNN "Sub  Family"
F 17 "12 V" H 4100 5750 50  0001 L CNN "UVLO Voltage"
F 18 "IC" H 4100 5850 50  0001 L CNN "category"
F 19 "8836869" H 4100 5950 50  0001 L CNN "ciiva ids"
F 20 "6a42b74abd90c7cc" H 4100 6050 50  0001 L CNN "library id"
F 21 "Silicon Labs" H 4100 6150 50  0001 L CNN "manufacturer"
F 22 "SDIP6" H 4100 6250 50  0001 L CNN "package"
F 23 "1382594481" H 4100 6350 50  0001 L CNN "release date"
F 24 "1913ADC0-D0E4-4EFC-85E7-4A1CDE3C3E5B" H 4100 6450 50  0001 L CNN "vault revision"
F 25 "yes" H 4100 6550 50  0001 L CNN "imported"
	1    4100 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3850 5700 3850
Wire Wire Line
	5700 4100 5700 3850
Connection ~ 5700 3850
Wire Wire Line
	5700 3850 6300 3850
$Comp
L power:GND #PWR013
U 1 1 5F52F45B
P 5350 4050
F 0 "#PWR013" H 5350 3800 50  0001 C CNN
F 1 "GND" H 5355 3877 50  0000 C CNN
F 2 "" H 5350 4050 50  0001 C CNN
F 3 "" H 5350 4050 50  0001 C CNN
	1    5350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4050 5200 4050
$Comp
L Device:C CF1
U 1 1 5F53DB83
P 3900 2650
F 0 "CF1" H 4015 2696 50  0000 L CNN
F 1 "100n" H 4015 2605 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 3938 2500 50  0001 C CNN
F 3 "~" H 3900 2650 50  0001 C CNN
	1    3900 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F53EE3A
P 4250 2950
F 0 "C1" H 4365 2996 50  0000 L CNN
F 1 "1u" H 4365 2905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4288 2800 50  0001 C CNN
F 3 "~" H 4250 2950 50  0001 C CNN
	1    4250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2800 3900 3250
Connection ~ 3900 3250
Wire Wire Line
	3900 3250 3750 3250
Wire Wire Line
	5700 4400 5700 4450
$Comp
L Device:C C5
U 1 1 5F5506E3
P 5950 2200
F 0 "C5" H 6065 2246 50  0000 L CNN
F 1 "1u" H 6065 2155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5988 2050 50  0001 C CNN
F 3 "~" H 5950 2200 50  0001 C CNN
	1    5950 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5F5518A6
P 5950 2750
F 0 "C6" H 6065 2796 50  0000 L CNN
F 1 "1u" H 6065 2705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5988 2600 50  0001 C CNN
F 3 "~" H 5950 2750 50  0001 C CNN
	1    5950 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5F55246E
P 6400 2200
F 0 "C11" H 6515 2246 50  0000 L CNN
F 1 "2.2u" H 6515 2155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6438 2050 50  0001 C CNN
F 3 "~" H 6400 2200 50  0001 C CNN
	1    6400 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5F55296E
P 6400 2750
F 0 "C12" H 6515 2796 50  0000 L CNN
F 1 "2.2u" H 6515 2705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6438 2600 50  0001 C CNN
F 3 "~" H 6400 2750 50  0001 C CNN
	1    6400 2750
	1    0    0    -1  
$EndComp
$Comp
L dk_Diodes-Zener-Single:1N5231BTR Z1
U 1 1 5F5536A7
P 6750 2700
F 0 "Z1" V 6803 2622 60  0000 R CNN
F 1 "1N5231BTR" V 6697 2622 60  0000 R CNN
F 2 "digikey-footprints:DO-35" H 6950 2900 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 6950 3000 60  0001 L CNN
F 4 "1N5231BFSCT-ND" H 6950 3100 60  0001 L CNN "Digi-Key_PN"
F 5 "1N5231BTR" H 6950 3200 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6950 3300 60  0001 L CNN "Category"
F 7 "Diodes - Zener - Single" H 6950 3400 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 6950 3500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/1N5231BTR/1N5231BFSCT-ND/1532765" H 6950 3600 60  0001 L CNN "DK_Detail_Page"
F 10 "DIODE ZENER 5.1V 500MW DO35" H 6950 3700 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 6950 3800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6950 3900 60  0001 L CNN "Status"
	1    6750 2700
	0    -1   -1   0   
$EndComp
$Comp
L dk_Diodes-Zener-Single:1N5231BTR Z3
U 1 1 5F5553F1
P 7000 2450
F 0 "Z3" V 7053 2372 60  0000 R CNN
F 1 "1N5231BTR" V 6947 2372 60  0000 R CNN
F 2 "digikey-footprints:DO-35" H 7200 2650 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7200 2750 60  0001 L CNN
F 4 "1N5231BFSCT-ND" H 7200 2850 60  0001 L CNN "Digi-Key_PN"
F 5 "1N5231BTR" H 7200 2950 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7200 3050 60  0001 L CNN "Category"
F 7 "Diodes - Zener - Single" H 7200 3150 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7200 3250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/1N5231BTR/1N5231BFSCT-ND/1532765" H 7200 3350 60  0001 L CNN "DK_Detail_Page"
F 10 "DIODE ZENER 5.1V 500MW DO35" H 7200 3450 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7200 3550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7200 3650 60  0001 L CNN "Status"
	1    7000 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 2450 5950 2350
Wire Wire Line
	5950 2450 5950 2600
Connection ~ 5950 2450
Wire Wire Line
	5950 2450 6200 2450
Wire Wire Line
	6400 2450 6400 2600
Wire Wire Line
	6400 2450 6400 2350
Connection ~ 6400 2450
Wire Wire Line
	5800 2350 5800 2050
Wire Wire Line
	5800 2050 5950 2050
Wire Wire Line
	5950 2050 6400 2050
Connection ~ 5950 2050
Wire Wire Line
	6400 2900 5950 2900
Wire Wire Line
	5950 2900 5800 2900
Wire Wire Line
	5800 2900 5800 2550
Connection ~ 5950 2900
Wire Wire Line
	6750 2900 6400 2900
Connection ~ 6400 2900
$Comp
L Device:R_US R7
U 1 1 5F56EF38
P 6750 2250
F 0 "R7" V 6545 2250 50  0000 C CNN
F 1 "47k" V 6636 2250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6790 2240 50  0001 C CNN
F 3 "~" H 6750 2250 50  0001 C CNN
	1    6750 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 2500 6750 2450
Wire Wire Line
	6400 2450 6750 2450
Connection ~ 6750 2450
Wire Wire Line
	6750 2450 6750 2400
Wire Wire Line
	6400 2050 6750 2050
Wire Wire Line
	6750 2050 6750 2100
Connection ~ 6400 2050
Wire Wire Line
	7000 2650 7000 2900
Wire Wire Line
	7000 2900 6750 2900
Connection ~ 6750 2900
Wire Wire Line
	7000 2250 7000 2050
Wire Wire Line
	7000 2050 6750 2050
Connection ~ 6750 2050
Wire Wire Line
	3950 4250 3950 4450
Wire Wire Line
	5700 4450 6250 4450
Wire Wire Line
	6600 4450 6600 4250
Connection ~ 5700 4450
Wire Wire Line
	6500 4250 6500 4450
Connection ~ 6500 4450
Wire Wire Line
	6500 4450 6600 4450
Wire Wire Line
	6500 3650 6500 3550
Wire Wire Line
	6600 3550 6600 3650
$Comp
L power:+15V #PWR09
U 1 1 5F5FEC62
P 4550 8150
F 0 "#PWR09" H 4550 8000 50  0001 C CNN
F 1 "+15V" H 4565 8323 50  0000 C CNN
F 2 "" H 4550 8150 50  0001 C CNN
F 3 "" H 4550 8150 50  0001 C CNN
	1    4550 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 4850 12700 4850
Wire Wire Line
	12700 4850 12700 4700
Connection ~ 12150 4850
$Comp
L power:+5V #PWR044
U 1 1 5F60C2DA
P 12700 4700
F 0 "#PWR044" H 12700 4550 50  0001 C CNN
F 1 "+5V" H 12715 4873 50  0000 C CNN
F 2 "" H 12700 4700 50  0001 C CNN
F 3 "" H 12700 4700 50  0001 C CNN
	1    12700 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4250 5200 4450
Wire Wire Line
	4200 4250 4150 4250
$Comp
L Device:C CF3
U 1 1 5F686146
P 4600 4450
F 0 "CF3" H 4715 4496 50  0000 L CNN
F 1 "100n" H 4715 4405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4638 4300 50  0001 C CNN
F 3 "~" H 4600 4450 50  0001 C CNN
	1    4600 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 4450 5200 4450
Wire Wire Line
	4150 4450 4150 4250
Connection ~ 4150 4250
Wire Wire Line
	4150 4250 3950 4250
$Comp
L Device:C C10
U 1 1 5F68F4A0
P 6250 4200
F 0 "C10" H 6365 4246 50  0000 L CNN
F 1 "1u" H 6365 4155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6288 4050 50  0001 C CNN
F 3 "~" H 6250 4200 50  0001 C CNN
	1    6250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3550 6250 3550
Wire Wire Line
	6250 3550 6250 4050
Connection ~ 6500 3550
Wire Wire Line
	6250 4350 6250 4450
Connection ~ 6250 4450
Wire Wire Line
	6250 4450 6500 4450
$Comp
L Device:D_Schottky D1
U 1 1 5F6996FF
P 7250 3500
F 0 "D1" H 7250 3717 50  0000 C CNN
F 1 "D_Schottky" H 7250 3626 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 7250 3500 50  0001 C CNN
F 3 "~" H 7250 3500 50  0001 C CNN
	1    7250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3850 6950 3850
Wire Wire Line
	6950 4050 6950 3850
Connection ~ 6950 3850
Wire Wire Line
	7600 3850 7850 3850
Wire Wire Line
	7550 3500 7400 3500
Wire Wire Line
	6950 3850 7100 3850
Wire Wire Line
	7100 3500 7100 3850
Connection ~ 7100 3850
Wire Wire Line
	7100 3850 7300 3850
Wire Wire Line
	7850 3850 7850 3500
Wire Wire Line
	4050 3850 4200 3850
Wire Wire Line
	4050 4050 4200 4050
$Comp
L Device:EMI_Filter_CommonMode FL2
U 1 1 5F75B36D
P 3650 5950
F 0 "FL2" H 3650 6231 50  0000 C CNN
F 1 "EMI_Filter_CommonMode" H 3650 6140 50  0000 C CNN
F 2 "BuckConverter:FIL_ACM4520-142-2P-T000" H 3650 5990 50  0001 C CNN
F 3 "~" H 3650 5990 50  0001 C CNN
	1    3650 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5F75B373
P 3450 6200
F 0 "#PWR04" H 3450 5950 50  0001 C CNN
F 1 "GND" H 3455 6027 50  0000 C CNN
F 2 "" H 3450 6200 50  0001 C CNN
F 3 "" H 3450 6200 50  0001 C CNN
	1    3450 6200
	1    0    0    -1  
$EndComp
$Comp
L dk_PMIC-Gate-Drivers:IXDN609SI U8
U 1 1 5F75B390
P 6700 6950
F 0 "U8" H 6356 7003 60  0000 R CNN
F 1 "IXDN609SI" H 6356 6897 60  0000 R CNN
F 2 "digikey-footprints:SOIC-8-1EP_W3.9mm" H 6900 7150 60  0001 L CNN
F 3 "http://www.ixysic.com/home/pdfs.nsf/www/IXD_609.pdf/$file/IXD_609.pdf" H 6900 7250 60  0001 L CNN
F 4 "CLA364-ND" H 6900 7350 60  0001 L CNN "Digi-Key_PN"
F 5 "IXDN609SI" H 6900 7450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6900 7550 60  0001 L CNN "Category"
F 7 "PMIC - Gate Drivers" H 6900 7650 60  0001 L CNN "Family"
F 8 "http://www.ixysic.com/home/pdfs.nsf/www/IXD_609.pdf/$file/IXD_609.pdf" H 6900 7750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ixys-integrated-circuits-division/IXDN609SI/CLA364-ND/2428208" H 6900 7850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC GATE DVR 9A NON-INV 8-SOIC" H 6900 7950 60  0001 L CNN "Description"
F 11 "IXYS Integrated Circuits Division" H 6900 8050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6900 8150 60  0001 L CNN "Status"
	1    6700 6950
	1    0    0    -1  
$EndComp
$Comp
L eec:Si8261BCD-C-IS U2
U 1 1 5F75B3AC
P 4200 6850
F 0 "U2" H 4800 7115 50  0000 C CNN
F 1 "Si8261BCD-C-IS" H 4800 7024 50  0000 C CNN
F 2 "BuckConverter:SOIC127P1150X265-6N" H 4200 7250 50  0001 L CNN
F 3 "http://www.silabs.com/Support%20Documents/TechnicalDocs/Si826x.pdf" H 4200 7350 50  0001 L CNN
F 4 "true" H 4200 7450 50  0001 L CNN "10kV surge"
F 5 "Manufacturer URL" H 4200 7550 50  0001 L CNN "Component Link 1 Description"
F 6 "http://www.silabs.com/" H 4200 7650 50  0001 L CNN "Component Link 1 URL"
F 7 "Package Specification" H 4200 7750 50  0001 L CNN "Component Link 3 Description"
F 8 "http://www.silabs.com/Support%20Documents/TechnicalDocs/Si826x.pdf" H 4200 7850 50  0001 L CNN "Component Link 3 URL"
F 9 "Rev. 0.9" H 4200 7950 50  0001 L CNN "Datasheet Version"
F 10 "5 to 30 V" H 4200 8050 50  0001 L CNN "Driver Supply"
F 11 "5" H 4200 8150 50  0001 L CNN "Isolation Rating Input Output kVrms"
F 12 "60" H 4200 8250 50  0001 L CNN "Max Prop Delay ns"
F 13 "Surface Mount" H 4200 8350 50  0001 L CNN "Mounting Technology"
F 14 "false" H 4200 8450 50  0001 L CNN "Overlap Protection  Dead Time Control"
F 15 "6-Pin SDIP, Body 4.58 x 7.5 mm, Pitch 1.27 mm" H 4200 8550 50  0001 L CNN "Package Description"
F 16 "ISOdrivers" H 4200 8650 50  0001 L CNN "Sub  Family"
F 17 "12 V" H 4200 8750 50  0001 L CNN "UVLO Voltage"
F 18 "IC" H 4200 8850 50  0001 L CNN "category"
F 19 "8836869" H 4200 8950 50  0001 L CNN "ciiva ids"
F 20 "6a42b74abd90c7cc" H 4200 9050 50  0001 L CNN "library id"
F 21 "Silicon Labs" H 4200 9150 50  0001 L CNN "manufacturer"
F 22 "SDIP6" H 4200 9250 50  0001 L CNN "package"
F 23 "1382594481" H 4200 9350 50  0001 L CNN "release date"
F 24 "1913ADC0-D0E4-4EFC-85E7-4A1CDE3C3E5B" H 4200 9450 50  0001 L CNN "vault revision"
F 25 "yes" H 4200 9550 50  0001 L CNN "imported"
	1    4200 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 7100 5800 6850
Connection ~ 5800 6850
Wire Wire Line
	5800 6850 6400 6850
$Comp
L power:GND #PWR014
U 1 1 5F75B3BC
P 5450 7050
F 0 "#PWR014" H 5450 6800 50  0001 C CNN
F 1 "GND" H 5455 6877 50  0000 C CNN
F 2 "" H 5450 7050 50  0001 C CNN
F 3 "" H 5450 7050 50  0001 C CNN
	1    5450 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 7050 5300 7050
$Comp
L Device:C CF2
U 1 1 5F75B3C6
P 4000 5450
F 0 "CF2" H 4115 5496 50  0000 L CNN
F 1 "100n" H 4115 5405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4038 5300 50  0001 C CNN
F 3 "~" H 4000 5450 50  0001 C CNN
	1    4000 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F75B3CC
P 4350 5750
F 0 "C2" H 4465 5796 50  0000 L CNN
F 1 "1u" H 4465 5705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4388 5600 50  0001 C CNN
F 3 "~" H 4350 5750 50  0001 C CNN
	1    4350 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 5600 4000 6050
Connection ~ 4000 6050
Wire Wire Line
	4000 6050 3850 6050
Wire Wire Line
	5800 7400 5800 7450
$Comp
L Device:C C7
U 1 1 5F75B3F0
P 6050 5000
F 0 "C7" H 6165 5046 50  0000 L CNN
F 1 "1u" H 6165 4955 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6088 4850 50  0001 C CNN
F 3 "~" H 6050 5000 50  0001 C CNN
	1    6050 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5F75B3F6
P 6050 5550
F 0 "C8" H 6165 5596 50  0000 L CNN
F 1 "1u" H 6165 5505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6088 5400 50  0001 C CNN
F 3 "~" H 6050 5550 50  0001 C CNN
	1    6050 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5F75B3FC
P 6500 5000
F 0 "C13" H 6615 5046 50  0000 L CNN
F 1 "2.2u" H 6615 4955 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6538 4850 50  0001 C CNN
F 3 "~" H 6500 5000 50  0001 C CNN
	1    6500 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5F75B402
P 6500 5550
F 0 "C14" H 6615 5596 50  0000 L CNN
F 1 "2.2u" H 6615 5505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6538 5400 50  0001 C CNN
F 3 "~" H 6500 5550 50  0001 C CNN
	1    6500 5550
	1    0    0    -1  
$EndComp
$Comp
L dk_Diodes-Zener-Single:1N5231BTR Z2
U 1 1 5F75B411
P 6850 5500
F 0 "Z2" V 6903 5422 60  0000 R CNN
F 1 "1N5231BTR" V 6797 5422 60  0000 R CNN
F 2 "digikey-footprints:DO-35" H 7050 5700 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7050 5800 60  0001 L CNN
F 4 "1N5231BFSCT-ND" H 7050 5900 60  0001 L CNN "Digi-Key_PN"
F 5 "1N5231BTR" H 7050 6000 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7050 6100 60  0001 L CNN "Category"
F 7 "Diodes - Zener - Single" H 7050 6200 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7050 6300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/1N5231BTR/1N5231BFSCT-ND/1532765" H 7050 6400 60  0001 L CNN "DK_Detail_Page"
F 10 "DIODE ZENER 5.1V 500MW DO35" H 7050 6500 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7050 6600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7050 6700 60  0001 L CNN "Status"
	1    6850 5500
	0    -1   -1   0   
$EndComp
$Comp
L dk_Diodes-Zener-Single:1N5231BTR Z4
U 1 1 5F75B420
P 7100 5250
F 0 "Z4" V 7153 5172 60  0000 R CNN
F 1 "1N5231BTR" V 7047 5172 60  0000 R CNN
F 2 "digikey-footprints:DO-35" H 7300 5450 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7300 5550 60  0001 L CNN
F 4 "1N5231BFSCT-ND" H 7300 5650 60  0001 L CNN "Digi-Key_PN"
F 5 "1N5231BTR" H 7300 5750 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7300 5850 60  0001 L CNN "Category"
F 7 "Diodes - Zener - Single" H 7300 5950 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/1N5221B-D.PDF" H 7300 6050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/1N5231BTR/1N5231BFSCT-ND/1532765" H 7300 6150 60  0001 L CNN "DK_Detail_Page"
F 10 "DIODE ZENER 5.1V 500MW DO35" H 7300 6250 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7300 6350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7300 6450 60  0001 L CNN "Status"
	1    7100 5250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 5250 6050 5150
Wire Wire Line
	6050 5250 6050 5400
Connection ~ 6050 5250
Wire Wire Line
	6050 5250 6350 5250
Wire Wire Line
	6500 5250 6500 5400
Wire Wire Line
	6500 5250 6500 5150
Connection ~ 6500 5250
Wire Wire Line
	5900 5150 5900 4850
Wire Wire Line
	5900 4850 6050 4850
Wire Wire Line
	6050 4850 6500 4850
Connection ~ 6050 4850
Wire Wire Line
	6500 5700 6050 5700
Wire Wire Line
	6050 5700 5900 5700
Wire Wire Line
	5900 5700 5900 5350
Connection ~ 6050 5700
Wire Wire Line
	6850 5700 6500 5700
Connection ~ 6500 5700
Wire Wire Line
	6850 5300 6850 5250
Wire Wire Line
	6500 5250 6850 5250
Connection ~ 6850 5250
Wire Wire Line
	6850 5250 6850 5200
Wire Wire Line
	6500 4850 6850 4850
Wire Wire Line
	6850 4850 6850 4900
Connection ~ 6500 4850
Wire Wire Line
	7100 5450 7100 5700
Wire Wire Line
	7100 5700 6850 5700
Connection ~ 6850 5700
Wire Wire Line
	7100 5050 7100 4850
Wire Wire Line
	7100 4850 6850 4850
Connection ~ 6850 4850
Wire Wire Line
	4050 7250 4050 7450
Wire Wire Line
	6700 7450 6700 7250
Connection ~ 5800 7450
Wire Wire Line
	6600 7250 6600 7450
Connection ~ 6600 7450
Wire Wire Line
	6600 7450 6700 7450
Wire Wire Line
	6600 6650 6600 6550
Wire Wire Line
	6600 6550 6650 6550
Wire Wire Line
	6700 6550 6700 6650
Wire Wire Line
	6650 6400 6650 6550
Connection ~ 6650 6550
Wire Wire Line
	6650 6550 6700 6550
$Comp
L Device:R_US R11
U 1 1 5F75B47B
P 7550 6850
F 0 "R11" V 7345 6850 50  0000 C CNN
F 1 "10" V 7436 6850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7590 6840 50  0001 C CNN
F 3 "" H 7550 6850 50  0001 C CNN
	1    7550 6850
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 6200 3450 6050
Wire Wire Line
	5300 7250 5300 7450
Wire Wire Line
	4300 7250 4250 7250
$Comp
L Device:C CF4
U 1 1 5F75B484
P 4700 7450
F 0 "CF4" H 4815 7496 50  0000 L CNN
F 1 "100n" H 4815 7405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4738 7300 50  0001 C CNN
F 3 "~" H 4700 7450 50  0001 C CNN
	1    4700 7450
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 7450 5300 7450
Connection ~ 5300 7450
Wire Wire Line
	4250 7450 4250 7250
Connection ~ 4250 7250
Wire Wire Line
	4250 7250 4050 7250
$Comp
L Device:C C9
U 1 1 5F75B491
P 6100 7250
F 0 "C9" H 6215 7296 50  0000 L CNN
F 1 "1u" H 6215 7205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6138 7100 50  0001 C CNN
F 3 "~" H 6100 7250 50  0001 C CNN
	1    6100 7250
	1    0    0    -1  
$EndComp
Connection ~ 6600 6550
$Comp
L Device:D_Schottky D2
U 1 1 5F75B49D
P 7350 6500
F 0 "D2" H 7350 6717 50  0000 C CNN
F 1 "D_Schottky" H 7350 6626 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 7350 6500 50  0001 C CNN
F 3 "~" H 7350 6500 50  0001 C CNN
	1    7350 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 6850 7050 6850
Wire Wire Line
	7050 7050 7050 6850
Connection ~ 7050 6850
$Comp
L Device:R_US R14
U 1 1 5F75B4A7
P 7800 6500
F 0 "R14" V 7595 6500 50  0000 C CNN
F 1 "10" V 7686 6500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7840 6490 50  0001 C CNN
F 3 "~" H 7800 6500 50  0001 C CNN
	1    7800 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	7700 6850 7950 6850
Wire Wire Line
	7650 6500 7500 6500
Wire Wire Line
	7050 6850 7200 6850
Wire Wire Line
	7200 6500 7200 6850
Connection ~ 7200 6850
Wire Wire Line
	7200 6850 7400 6850
Wire Wire Line
	7950 6850 7950 6500
Wire Wire Line
	4150 6850 4300 6850
Wire Wire Line
	4150 7050 4300 7050
Wire Wire Line
	8650 6300 8650 6850
Wire Wire Line
	8650 6850 8400 6850
Connection ~ 7950 6850
$Comp
L power:GND #PWR023
U 1 1 5F7AE94A
P 6350 5350
F 0 "#PWR023" H 6350 5100 50  0001 C CNN
F 1 "GND" H 6355 5177 50  0000 C CNN
F 2 "" H 6350 5350 50  0001 C CNN
F 3 "" H 6350 5350 50  0001 C CNN
	1    6350 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5350 6350 5250
Connection ~ 6350 5250
Wire Wire Line
	6350 5250 6500 5250
$Comp
L Device:C C3
U 1 1 5F7E341F
P 4650 8700
F 0 "C3" H 4765 8746 50  0000 L CNN
F 1 "0.33u" H 4765 8655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4688 8550 50  0001 C CNN
F 3 "~" H 4650 8700 50  0001 C CNN
	1    4650 8700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5F7E3B80
P 5700 8700
F 0 "C4" H 5815 8746 50  0000 L CNN
F 1 "0.1u" H 5815 8655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5738 8550 50  0001 C CNN
F 3 "~" H 5700 8700 50  0001 C CNN
	1    5700 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 8850 4650 8950
Connection ~ 4650 8950
Wire Wire Line
	4650 8950 5250 8950
Wire Wire Line
	4650 8550 4650 8400
Connection ~ 4650 8400
Wire Wire Line
	4650 8400 4950 8400
Wire Wire Line
	5700 8550 5700 8400
Connection ~ 5700 8400
Wire Wire Line
	5700 8400 5550 8400
Wire Wire Line
	5700 8850 5700 8950
Connection ~ 5700 8950
Wire Wire Line
	12500 5050 12800 5050
Connection ~ 12500 5050
$Comp
L power:GND #PWR033
U 1 1 5F83A70B
P 9950 5150
F 0 "#PWR033" H 9950 4900 50  0001 C CNN
F 1 "GND" H 9955 4977 50  0000 C CNN
F 2 "" H 9950 5150 50  0001 C CNN
F 3 "" H 9950 5150 50  0001 C CNN
	1    9950 5150
	1    0    0    -1  
$EndComp
$Comp
L power:LINE #PWR028
U 1 1 5F83DAD5
P 8500 5600
F 0 "#PWR028" H 8500 5450 50  0001 C CNN
F 1 "LINE" H 8515 5773 50  0000 C CNN
F 2 "" H 8500 5600 50  0001 C CNN
F 3 "" H 8500 5600 50  0001 C CNN
	1    8500 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 5800 8500 5800
Wire Wire Line
	8500 5800 8500 5600
$Comp
L power:LINE #PWR022
U 1 1 5F84E03E
P 6200 2400
F 0 "#PWR022" H 6200 2250 50  0001 C CNN
F 1 "LINE" H 6215 2573 50  0000 C CNN
F 2 "" H 6200 2400 50  0001 C CNN
F 3 "" H 6200 2400 50  0001 C CNN
	1    6200 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2400 6200 2450
Connection ~ 6200 2450
Wire Wire Line
	6200 2450 6400 2450
Wire Wire Line
	3350 3250 3350 3300
Wire Wire Line
	7850 3850 8200 3850
Wire Wire Line
	8500 3850 8500 5250
Wire Wire Line
	8500 5250 8650 5250
Connection ~ 7850 3850
Wire Wire Line
	5250 9050 5250 8950
$Comp
L inv_1p_cc-rescue:LM358AD-mycomp U10
U 1 1 5F5618FB
P 10850 3650
F 0 "U10" H 11400 3915 50  0000 C CNN
F 1 "LM358AD-mycomp" H 11400 3824 50  0000 C CNN
F 2 "BuckConverter:SOIC127P600X175-8N" H 11800 3750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358a.pdf" H 11800 3650 50  0001 L CNN
F 4 "LM358ADG4, Dual Operational Amplifier 0.7MHz 5 to 28V, 8-Pin SOIC" H 11800 3550 50  0001 L CNN "Description"
F 5 "1.75" H 11800 3450 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 11800 3350 50  0001 L CNN "Manufacturer_Name"
F 7 "LM358AD" H 11800 3250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-LM358AD" H 11800 3150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-LM358AD" H 11800 3050 50  0001 L CNN "Mouser Price/Stock"
F 10 "6610546P" H 11800 2950 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/6610546P" H 11800 2850 50  0001 L CNN "RS Price/Stock"
	1    10850 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5F593991
P 3750 7150
F 0 "#PWR06" H 3750 6900 50  0001 C CNN
F 1 "GND" H 3755 6977 50  0000 C CNN
F 2 "" H 3750 7150 50  0001 C CNN
F 3 "" H 3750 7150 50  0001 C CNN
	1    3750 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F593F76
P 3650 4150
F 0 "#PWR05" H 3650 3900 50  0001 C CNN
F 1 "GND" H 3655 3977 50  0000 C CNN
F 2 "" H 3650 4150 50  0001 C CNN
F 3 "" H 3650 4150 50  0001 C CNN
	1    3650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 7150 3750 7050
Wire Wire Line
	3750 7050 3850 7050
Wire Wire Line
	3650 4150 3650 4050
Wire Wire Line
	3650 4050 3750 4050
Wire Wire Line
	3450 5850 3450 5550
$Comp
L power:+15V #PWR03
U 1 1 5F5C5AAA
P 3450 5550
F 0 "#PWR03" H 3450 5400 50  0001 C CNN
F 1 "+15V" H 3465 5723 50  0000 C CNN
F 2 "" H 3450 5550 50  0001 C CNN
F 3 "" H 3450 5550 50  0001 C CNN
	1    3450 5550
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR01
U 1 1 5F5C6399
P 3350 2800
F 0 "#PWR01" H 3350 2650 50  0001 C CNN
F 1 "+15V" H 3365 2973 50  0000 C CNN
F 2 "" H 3350 2800 50  0001 C CNN
F 3 "" H 3350 2800 50  0001 C CNN
	1    3350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3050 3350 2800
$Comp
L Converter_DCDC:MGJ2D151505SC U4
U 1 1 5F5DB685
P 5050 5250
F 0 "U4" H 5050 5717 50  0000 C CNN
F 1 "MGJ2D151505SC" H 5050 5626 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MGJ2DxxxxxxSC_THT" H 5050 4850 50  0001 C CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mgj2.pdf" H 5025 5250 50  0001 C CNN
	1    5050 5250
	1    0    0    -1  
$EndComp
$Comp
L Converter_DCDC:MGJ2D151505SC U3
U 1 1 5F5DCBDE
P 4950 2450
F 0 "U3" H 4950 2917 50  0000 C CNN
F 1 "MGJ2D151505SC" H 4950 2826 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MGJ2DxxxxxxSC_THT" H 4950 2050 50  0001 C CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mgj2.pdf" H 4925 2450 50  0001 C CNN
	1    4950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 5050 5600 5050
Wire Wire Line
	5600 5050 5600 5150
Wire Wire Line
	5550 5250 6050 5250
Wire Wire Line
	5550 5350 5550 5450
Wire Wire Line
	4550 6050 4550 5450
Wire Wire Line
	3850 5050 3850 5850
Wire Wire Line
	4000 5050 4000 5300
Wire Wire Line
	3850 5050 4000 5050
Connection ~ 4000 5050
Wire Wire Line
	4000 5050 4200 5050
Wire Wire Line
	4350 5600 4350 5050
Connection ~ 4350 5050
Wire Wire Line
	4350 5050 4550 5050
Wire Wire Line
	4350 5900 4350 6050
Connection ~ 4350 6050
Wire Wire Line
	5450 2450 5950 2450
Wire Wire Line
	5500 2350 5500 2250
Wire Wire Line
	5500 2250 5450 2250
Wire Wire Line
	5500 2550 5500 2650
Wire Wire Line
	5500 2650 5450 2650
Wire Wire Line
	3900 3250 4250 3250
Wire Wire Line
	3750 2250 3750 3050
Wire Wire Line
	3900 2250 3900 2500
Connection ~ 3900 2250
Wire Wire Line
	3900 2250 3750 2250
Wire Wire Line
	4250 2800 4250 2250
Connection ~ 4250 2250
Wire Wire Line
	4250 2250 4100 2250
Wire Wire Line
	4250 3100 4250 3250
Connection ~ 4250 3250
Wire Wire Line
	4450 3250 4450 2650
Wire Wire Line
	4250 3250 4450 3250
$Comp
L power:GND #PWR043
U 1 1 5F7A591D
P 14000 2800
F 0 "#PWR043" H 14000 2550 50  0001 C CNN
F 1 "GND" H 14005 2627 50  0000 C CNN
F 2 "" H 14000 2800 50  0001 C CNN
F 3 "" H 14000 2800 50  0001 C CNN
	1    14000 2800
	1    0    0    -1  
$EndComp
Text GLabel 14050 1750 0    50   Output ~ 0
PWM1
Text GLabel 14050 2200 0    50   Output ~ 0
PWM2
Text GLabel 14050 2600 0    50   Input ~ 0
Vo_meas
Text GLabel 14050 3150 0    50   Input ~ 0
Vd_meas
Text GLabel 14050 2700 0    50   Input ~ 0
Io_meas
Text GLabel 3750 3850 0    50   Input ~ 0
PWM1
Text GLabel 3850 6850 0    50   Input ~ 0
PWM2
Wire Wire Line
	11950 5450 12500 5450
Wire Wire Line
	10750 5800 10750 5250
Wire Wire Line
	10750 5050 10200 5050
$Comp
L power:GND #PWR042
U 1 1 5F810BAD
P 12500 5550
F 0 "#PWR042" H 12500 5300 50  0001 C CNN
F 1 "GND" H 12505 5377 50  0000 C CNN
F 2 "" H 12500 5550 50  0001 C CNN
F 3 "" H 12500 5550 50  0001 C CNN
	1    12500 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12500 5550 12500 5450
Connection ~ 12500 5450
Wire Wire Line
	8850 2500 8850 2650
Text GLabel 12800 5050 2    50   Output ~ 0
Io
Wire Wire Line
	12250 5900 12850 5900
Connection ~ 12250 5900
Wire Wire Line
	12250 5900 12250 6300
Text GLabel 12850 5900 2    50   Output ~ 0
Vo
Wire Wire Line
	10850 3750 10750 3750
Wire Wire Line
	10750 3750 10750 3650
Wire Wire Line
	10750 3650 10850 3650
Wire Wire Line
	10850 2900 10800 2900
Wire Wire Line
	10800 2900 10800 2800
Wire Wire Line
	10800 2800 10850 2800
$Comp
L Device:R_US R23
U 1 1 5F9268BB
P 10200 3000
F 0 "R23" V 9995 3000 50  0000 C CNN
F 1 "100k" V 10086 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10240 2990 50  0001 C CNN
F 3 "~" H 10200 3000 50  0001 C CNN
	1    10200 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R25
U 1 1 5F93BD4F
P 10550 3200
F 0 "R25" V 10345 3200 50  0000 C CNN
F 1 "10k" V 10436 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10590 3190 50  0001 C CNN
F 3 "~" H 10550 3200 50  0001 C CNN
	1    10550 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 3050 10550 3000
Connection ~ 10550 3000
Wire Wire Line
	10550 3000 10350 3000
$Comp
L power:GND #PWR035
U 1 1 5F9740CD
P 10550 3350
F 0 "#PWR035" H 10550 3100 50  0001 C CNN
F 1 "GND" H 10555 3177 50  0000 C CNN
F 2 "" H 10550 3350 50  0001 C CNN
F 3 "" H 10550 3350 50  0001 C CNN
	1    10550 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R21
U 1 1 5F974F6D
P 9900 3000
F 0 "R21" V 9695 3000 50  0000 C CNN
F 1 "100k" V 9786 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9940 2990 50  0001 C CNN
F 3 "~" H 9900 3000 50  0001 C CNN
	1    9900 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R20
U 1 1 5F975159
P 9600 3000
F 0 "R20" V 9395 3000 50  0000 C CNN
F 1 "100k" V 9486 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9640 2990 50  0001 C CNN
F 3 "~" H 9600 3000 50  0001 C CNN
	1    9600 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R19
U 1 1 5F97537A
P 9300 3000
F 0 "R19" V 9095 3000 50  0000 C CNN
F 1 "100k" V 9186 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9340 2990 50  0001 C CNN
F 3 "~" H 9300 3000 50  0001 C CNN
	1    9300 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R18
U 1 1 5F975602
P 9000 3000
F 0 "R18" V 8795 3000 50  0000 C CNN
F 1 "100k" V 8886 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9040 2990 50  0001 C CNN
F 3 "~" H 9000 3000 50  0001 C CNN
	1    9000 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 3000 10550 3000
$Comp
L Device:R_US R17
U 1 1 5F99225C
P 8700 3000
F 0 "R17" V 8495 3000 50  0000 C CNN
F 1 "100k" V 8586 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8740 2990 50  0001 C CNN
F 3 "~" H 8700 3000 50  0001 C CNN
	1    8700 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R16
U 1 1 5F992262
P 8400 3000
F 0 "R16" V 8195 3000 50  0000 C CNN
F 1 "100k" V 8286 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8440 2990 50  0001 C CNN
F 3 "~" H 8400 3000 50  0001 C CNN
	1    8400 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R15
U 1 1 5F992268
P 8100 3000
F 0 "R15" V 7895 3000 50  0000 C CNN
F 1 "100k" V 7986 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8140 2990 50  0001 C CNN
F 3 "~" H 8100 3000 50  0001 C CNN
	1    8100 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R13
U 1 1 5F99226E
P 7800 3000
F 0 "R13" V 7595 3000 50  0000 C CNN
F 1 "100k" V 7686 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7840 2990 50  0001 C CNN
F 3 "~" H 7800 3000 50  0001 C CNN
	1    7800 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R10
U 1 1 5F992274
P 7500 3000
F 0 "R10" V 7295 3000 50  0000 C CNN
F 1 "100k" V 7386 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7540 2990 50  0001 C CNN
F 3 "~" H 7500 3000 50  0001 C CNN
	1    7500 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 2800 10800 2350
Wire Wire Line
	10800 2350 11100 2350
Connection ~ 10800 2800
Text GLabel 11100 2350 2    50   Output ~ 0
Vd_meas
$Comp
L Device:R_US R22
U 1 1 5F577232
P 10050 3850
F 0 "R22" V 9845 3850 50  0000 C CNN
F 1 "10k" V 9936 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10090 3840 50  0001 C CNN
F 3 "~" H 10050 3850 50  0001 C CNN
	1    10050 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R24
U 1 1 5F57762E
P 10500 4000
F 0 "R24" V 10295 4000 50  0000 C CNN
F 1 "15k" V 10386 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10540 3990 50  0001 C CNN
F 3 "~" H 10500 4000 50  0001 C CNN
	1    10500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3850 10850 3850
Wire Wire Line
	10500 3850 10200 3850
Connection ~ 10500 3850
Text GLabel 9900 3850 0    50   Input ~ 0
Io
$Comp
L power:GND #PWR034
U 1 1 5F5FD2B9
P 10500 4150
F 0 "#PWR034" H 10500 3900 50  0001 C CNN
F 1 "GND" H 10505 3977 50  0000 C CNN
F 2 "" H 10500 4150 50  0001 C CNN
F 3 "" H 10500 4150 50  0001 C CNN
	1    10500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 3650 10750 3350
Wire Wire Line
	10750 3350 12150 3350
Connection ~ 10750 3650
Text GLabel 12150 3350 2    50   Output ~ 0
Io_meas
$Comp
L Device:R_US R26
U 1 1 5F6123B0
P 12500 4100
F 0 "R26" V 12295 4100 50  0000 C CNN
F 1 "6.8k" V 12386 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12540 4090 50  0001 C CNN
F 3 "~" H 12500 4100 50  0001 C CNN
	1    12500 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R27
U 1 1 5F612783
P 12850 3950
F 0 "R27" V 12645 3950 50  0000 C CNN
F 1 "100k" V 12736 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12890 3940 50  0001 C CNN
F 3 "~" H 12850 3950 50  0001 C CNN
	1    12850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 3850 12050 3850
Wire Wire Line
	12050 3850 12050 3750
Wire Wire Line
	12050 3750 11950 3750
$Comp
L power:GND #PWR041
U 1 1 5F6285D2
P 12500 4250
F 0 "#PWR041" H 12500 4000 50  0001 C CNN
F 1 "GND" H 12505 4077 50  0000 C CNN
F 2 "" H 12500 4250 50  0001 C CNN
F 3 "" H 12500 4250 50  0001 C CNN
	1    12500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11950 3950 12500 3950
Wire Wire Line
	12500 3950 12700 3950
Connection ~ 12500 3950
Text GLabel 13000 3950 2    50   Input ~ 0
Vo
Wire Wire Line
	12050 3750 12450 3750
Connection ~ 12050 3750
Text GLabel 12450 3750 2    50   Output ~ 0
Vo_meas
Wire Wire Line
	5800 7450 6100 7450
Wire Wire Line
	6100 7400 6100 7450
Connection ~ 6100 7450
Wire Wire Line
	6100 7450 6600 7450
Wire Wire Line
	6100 7100 6100 6550
Wire Wire Line
	6100 6550 6600 6550
$Comp
L MEJ2D0503SC:MEJ2D0503SC PS1
U 1 1 5F6C1CF2
P 6350 8200
F 0 "PS1" H 7178 8046 50  0000 L CNN
F 1 "MEJ2D0503SC" H 7178 7955 50  0000 L CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MGJ2DxxxxxxSC_THT" H 7200 8300 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/8294935P" H 7200 8200 50  0001 L CNN
F 4 "Murata Power Solutions MEJ2 2W Isolated DC-DC Converter Through Hole, Vin 4.5  5.5 V dc, Vout +/-3.3V dc" H 7200 8100 50  0001 L CNN "Description"
F 5 "12.65" H 7200 8000 50  0001 L CNN "Height"
F 6 "8294935P" H 7200 7900 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/8294935P" H 7200 7800 50  0001 L CNN "RS Price/Stock"
F 8 "Murata Electronics" H 7200 7700 50  0001 L CNN "Manufacturer_Name"
F 9 "MEJ2D0503SC" H 7200 7600 50  0001 L CNN "Manufacturer_Part_Number"
	1    6350 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 8500 6300 8500
Wire Wire Line
	6300 8500 6300 8950
Wire Wire Line
	5700 8950 6200 8950
Wire Wire Line
	5900 8250 6000 8250
Wire Wire Line
	6000 8250 6000 8100
Wire Wire Line
	6000 8100 6350 8100
Wire Wire Line
	6350 8100 6350 8200
Connection ~ 5900 8250
Wire Wire Line
	5900 8250 5900 8400
Wire Wire Line
	6350 8300 6200 8300
Wire Wire Line
	6200 8300 6200 8950
Connection ~ 6200 8950
Wire Wire Line
	6200 8950 6300 8950
$Comp
L Device:C C17
U 1 1 5F839308
P 7850 4250
F 0 "C17" H 7965 4296 50  0000 L CNN
F 1 "10n" H 7965 4205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7888 4100 50  0001 C CNN
F 3 "~" H 7850 4250 50  0001 C CNN
	1    7850 4250
	0    1    1    0   
$EndComp
$Comp
L Device:C C18
U 1 1 5F839E94
P 7850 4600
F 0 "C18" H 7965 4646 50  0000 L CNN
F 1 "100n" H 7965 4555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7888 4450 50  0001 C CNN
F 3 "~" H 7850 4600 50  0001 C CNN
	1    7850 4600
	0    1    1    0   
$EndComp
$Comp
L Device:C C19
U 1 1 5F83A3D5
P 7850 4950
F 0 "C19" H 7965 4996 50  0000 L CNN
F 1 "4.7u" H 7965 4905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7888 4800 50  0001 C CNN
F 3 "~" H 7850 4950 50  0001 C CNN
	1    7850 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 3950 8200 3850
Connection ~ 8200 3850
Wire Wire Line
	8200 3850 8500 3850
Wire Wire Line
	8200 4250 8000 4250
Wire Wire Line
	8000 4600 8200 4600
Wire Wire Line
	8200 4600 8200 4250
Connection ~ 8200 4250
Wire Wire Line
	8000 4950 8200 4950
Wire Wire Line
	8200 4950 8200 4600
Connection ~ 8200 4600
Wire Wire Line
	7700 4950 7700 4600
Wire Wire Line
	7700 4250 7700 4600
Connection ~ 7700 4600
Wire Wire Line
	7700 4600 7450 4600
$Comp
L power:LINE #PWR047
U 1 1 5F8D722F
P 8300 4600
F 0 "#PWR047" H 8300 4450 50  0001 C CNN
F 1 "LINE" H 8315 4773 50  0000 C CNN
F 2 "" H 8300 4600 50  0001 C CNN
F 3 "" H 8300 4600 50  0001 C CNN
	1    8300 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 4600 8300 4600
$Comp
L Device:C C20
U 1 1 5F9092AD
P 8050 7250
F 0 "C20" H 8165 7296 50  0000 L CNN
F 1 "10n" H 8165 7205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8088 7100 50  0001 C CNN
F 3 "~" H 8050 7250 50  0001 C CNN
	1    8050 7250
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 5F9092B3
P 8050 7600
F 0 "C21" H 8165 7646 50  0000 L CNN
F 1 "100n" H 8165 7555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8088 7450 50  0001 C CNN
F 3 "~" H 8050 7600 50  0001 C CNN
	1    8050 7600
	0    1    1    0   
$EndComp
$Comp
L Device:C C22
U 1 1 5F9092B9
P 8050 7950
F 0 "C22" H 8165 7996 50  0000 L CNN
F 1 "4.7u" H 8165 7905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8088 7800 50  0001 C CNN
F 3 "~" H 8050 7950 50  0001 C CNN
	1    8050 7950
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R29
U 1 1 5F9092BF
P 8400 7100
F 0 "R29" V 8195 7100 50  0000 C CNN
F 1 "47k" V 8286 7100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8440 7090 50  0001 C CNN
F 3 "~" H 8400 7100 50  0001 C CNN
	1    8400 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 7250 8200 7250
Wire Wire Line
	8200 7600 8400 7600
Wire Wire Line
	8400 7600 8400 7250
Connection ~ 8400 7250
Wire Wire Line
	8200 7950 8400 7950
Wire Wire Line
	8400 7950 8400 7600
Connection ~ 8400 7600
Wire Wire Line
	7900 7950 7900 7600
Wire Wire Line
	7900 7250 7900 7600
Connection ~ 7900 7600
Wire Wire Line
	7900 7600 7650 7600
Wire Wire Line
	8400 7600 8500 7600
Wire Wire Line
	8400 6950 8400 6850
Connection ~ 8400 6850
Wire Wire Line
	8400 6850 7950 6850
$Comp
L power:GND #PWR048
U 1 1 5F94FCDF
P 8500 7600
F 0 "#PWR048" H 8500 7350 50  0001 C CNN
F 1 "GND" H 8505 7427 50  0000 C CNN
F 2 "" H 8500 7600 50  0001 C CNN
F 3 "" H 8500 7600 50  0001 C CNN
	1    8500 7600
	0    -1   -1   0   
$EndComp
$Comp
L power:VPP #PWR0102
U 1 1 5F65EA2E
P 8850 2200
F 0 "#PWR0102" H 8850 2050 50  0001 C CNN
F 1 "VPP" H 8865 2373 50  0000 C CNN
F 2 "" H 8850 2200 50  0001 C CNN
F 3 "" H 8850 2200 50  0001 C CNN
	1    8850 2200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5F668325
P 9150 4100
F 0 "#FLG0105" H 9150 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 9150 4273 50  0000 C CNN
F 2 "" H 9150 4100 50  0001 C CNN
F 3 "~" H 9150 4100 50  0001 C CNN
	1    9150 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 8400 4550 8400
NoConn ~ 6800 7250
NoConn ~ 6700 4250
NoConn ~ 11950 2900
NoConn ~ 11950 3000
NoConn ~ 11950 3100
Wire Wire Line
	5300 7450 5750 7450
Wire Wire Line
	5600 5150 5900 5150
Wire Wire Line
	6650 6400 5600 6400
Wire Wire Line
	5600 6400 5600 5150
Connection ~ 5600 5150
Wire Wire Line
	5550 5350 5750 5350
Wire Wire Line
	5750 5350 5750 7450
Connection ~ 5750 5350
Wire Wire Line
	5750 5350 5900 5350
Connection ~ 5750 7450
Wire Wire Line
	5750 7450 5800 7450
Wire Wire Line
	4250 2250 4450 2250
Wire Wire Line
	5500 2550 5800 2550
Wire Wire Line
	5500 2350 5550 2350
Wire Wire Line
	5550 2350 5550 3550
Wire Wire Line
	5550 3550 6250 3550
Connection ~ 5550 2350
Wire Wire Line
	5550 2350 5800 2350
Connection ~ 6250 3550
Wire Wire Line
	6500 3550 6600 3550
Wire Wire Line
	5500 2650 5500 4450
Wire Wire Line
	5500 4450 5700 4450
Connection ~ 5500 2650
Wire Wire Line
	5200 4450 5500 4450
Connection ~ 5200 4450
Connection ~ 5500 4450
Wire Wire Line
	6600 4450 7450 4450
Wire Wire Line
	7450 4450 7450 4600
Connection ~ 6600 4450
Wire Wire Line
	6700 7450 7650 7450
Wire Wire Line
	7650 7450 7650 7600
Connection ~ 6700 7450
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5F837A72
P 4200 5050
F 0 "#FLG0103" H 4200 5125 50  0001 C CNN
F 1 "PWR_FLAG" H 4200 5223 50  0000 C CNN
F 2 "" H 4200 5050 50  0001 C CNN
F 3 "~" H 4200 5050 50  0001 C CNN
	1    4200 5050
	-1   0    0    1   
$EndComp
Connection ~ 4200 5050
Wire Wire Line
	4200 5050 4350 5050
Wire Wire Line
	4450 4450 4150 4450
$Comp
L power:PWR_FLAG #FLG0106
U 1 1 5F83A952
P 4100 2250
F 0 "#FLG0106" H 4100 2325 50  0001 C CNN
F 1 "PWR_FLAG" H 4100 2423 50  0000 C CNN
F 2 "" H 4100 2250 50  0001 C CNN
F 3 "~" H 4100 2250 50  0001 C CNN
	1    4100 2250
	-1   0    0    1   
$EndComp
Connection ~ 4100 2250
Wire Wire Line
	4100 2250 3900 2250
$Comp
L power:+3.3V #PWR0103
U 1 1 5F83F3AE
P 6350 8850
F 0 "#PWR0103" H 6350 8700 50  0001 C CNN
F 1 "+3.3V" H 6365 9023 50  0000 C CNN
F 2 "" H 6350 8850 50  0001 C CNN
F 3 "" H 6350 8850 50  0001 C CNN
	1    6350 8850
	-1   0    0    1   
$EndComp
$Comp
L power:-3V3 #PWR0104
U 1 1 5F85A97F
P 6050 8450
F 0 "#PWR0104" H 6050 8550 50  0001 C CNN
F 1 "-3V3" H 6065 8623 50  0000 C CNN
F 2 "" H 6050 8450 50  0001 C CNN
F 3 "" H 6050 8450 50  0001 C CNN
	1    6050 8450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 8400 6050 8450
NoConn ~ 6900 4050
NoConn ~ 7000 7050
$Comp
L power:+3V3 #PWR0101
U 1 1 5F6AFC1B
P 11950 2800
F 0 "#PWR0101" H 11950 2650 50  0001 C CNN
F 1 "+3V3" H 11965 2973 50  0000 C CNN
F 2 "" H 11950 2800 50  0001 C CNN
F 3 "" H 11950 2800 50  0001 C CNN
	1    11950 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5F6B0EFB
P 11950 3650
F 0 "#PWR0105" H 11950 3500 50  0001 C CNN
F 1 "+3.3V" H 11965 3823 50  0000 C CNN
F 2 "" H 11950 3650 50  0001 C CNN
F 3 "" H 11950 3650 50  0001 C CNN
	1    11950 3650
	1    0    0    -1  
$EndComp
$Comp
L power:-3V3 #PWR0106
U 1 1 5F6B15CF
P 10850 3950
F 0 "#PWR0106" H 10850 4050 50  0001 C CNN
F 1 "-3V3" H 10865 4123 50  0000 C CNN
F 2 "" H 10850 3950 50  0001 C CNN
F 3 "" H 10850 3950 50  0001 C CNN
	1    10850 3950
	-1   0    0    1   
$EndComp
$Comp
L power:-3V3 #PWR0107
U 1 1 5F6B3871
P 10850 3100
F 0 "#PWR0107" H 10850 3200 50  0001 C CNN
F 1 "-3V3" H 10865 3273 50  0000 C CNN
F 2 "" H 10850 3100 50  0001 C CNN
F 3 "" H 10850 3100 50  0001 C CNN
	1    10850 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 6050 4550 6050
Wire Wire Line
	6050 8400 6350 8400
Wire Wire Line
	6350 8600 6350 8850
Wire Wire Line
	11350 6650 12250 6650
$Comp
L BuckConverter-rescue:MKP1848C62550JP4-MKP1848C62550JP4-BuckConverter-rescue C16
U 1 1 5F7932CA
P 11250 6650
F 0 "C16" V 11696 6422 50  0000 R CNN
F 1 "47uF" V 11605 6422 50  0000 R CNN
F 2 "BuckConverter:MKP" H 11900 6750 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/mkp1848cdclink-461049.pdf" H 11900 6650 50  0001 L CNN
F 4 "Film Capacitors MKP , 25 uF , 5% , 500Vdc, pitch 37,5 , 4 pins" H 11900 6550 50  0001 L CNN "Description"
F 5 "38.5" H 11900 6450 50  0001 L CNN "Height"
F 6 "Vishay" H 11900 6350 50  0001 L CNN "Manufacturer_Name"
F 7 "MKP1848C62550JP4" H 11900 6250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "MKP1848C62550JP4" H 11900 6150 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/mkp1848c62550jp4/vishay" H 11900 6050 50  0001 L CNN "Arrow Price/Stock"
F 10 "N/A" H 11900 5950 50  0001 L CNN "Mouser Part Number"
F 11 "https://www.mouser.com/Search/Refine.aspx?Keyword=N%2FA" H 11900 5850 50  0001 L CNN "Mouser Price/Stock"
	1    11250 6650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 6850 5800 6850
Wire Wire Line
	5600 6400 3550 6400
Wire Wire Line
	3550 6400 3550 7450
Wire Wire Line
	3550 7450 4050 7450
Connection ~ 5600 6400
Wire Wire Line
	4250 7450 4550 7450
Wire Wire Line
	5550 3550 3450 3550
Wire Wire Line
	3450 3550 3450 4450
Wire Wire Line
	3450 4450 3950 4450
Connection ~ 5550 3550
Wire Wire Line
	10750 5800 11250 5800
Wire Wire Line
	11250 5850 11250 5800
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5F80EE14
P 14250 2200
F 0 "J5" H 14330 2192 50  0000 L CNN
F 1 "Conn_01x02" H 14330 2101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 14250 2200 50  0001 C CNN
F 3 "~" H 14250 2200 50  0001 C CNN
	1    14250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 6650 11250 6650
Wire Wire Line
	11350 5850 11350 5800
Wire Wire Line
	11350 5800 12250 5800
$Comp
L power:GND #PWR07
U 1 1 5F81136E
P 14050 1850
F 0 "#PWR07" H 14050 1600 50  0001 C CNN
F 1 "GND" H 14055 1677 50  0000 C CNN
F 2 "" H 14050 1850 50  0001 C CNN
F 3 "" H 14050 1850 50  0001 C CNN
	1    14050 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F810616
P 14050 2300
F 0 "#PWR08" H 14050 2050 50  0001 C CNN
F 1 "GND" H 14055 2127 50  0000 C CNN
F 2 "" H 14050 2300 50  0001 C CNN
F 3 "" H 14050 2300 50  0001 C CNN
	1    14050 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5F80F513
P 14250 1750
F 0 "J4" H 14330 1742 50  0000 L CNN
F 1 "Conn_01x02" H 14330 1651 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 14250 1750 50  0001 C CNN
F 3 "~" H 14250 1750 50  0001 C CNN
	1    14250 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5F8C68F3
P 14250 3150
F 0 "J6" H 14330 3142 50  0000 L CNN
F 1 "Conn_01x02" H 14330 3051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 14250 3150 50  0001 C CNN
F 3 "~" H 14250 3150 50  0001 C CNN
	1    14250 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 5F8C8655
P 14250 2700
F 0 "J7" H 14330 2742 50  0000 L CNN
F 1 "Conn_01x03" H 14330 2651 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 14250 2700 50  0001 C CNN
F 3 "~" H 14250 2700 50  0001 C CNN
	1    14250 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5F8CA71E
P 14000 3250
F 0 "#PWR010" H 14000 3000 50  0001 C CNN
F 1 "GND" H 14005 3077 50  0000 C CNN
F 2 "" H 14000 3250 50  0001 C CNN
F 3 "" H 14000 3250 50  0001 C CNN
	1    14000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 3250 14050 3250
Wire Wire Line
	14000 2800 14050 2800
$Comp
L BuckConverter-rescue:MKP1848C62550JP4-MKP1848C62550JP4-BuckConverter-rescue C15
U 1 1 5F7D9A1A
P 9400 5150
F 0 "C15" V 9846 4922 50  0000 R CNN
F 1 "MKP1848C62550JP4" V 9755 4922 50  0000 R CNN
F 2 "BuckConverter:MKP" H 10050 5250 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/mkp1848cdclink-461049.pdf" H 10050 5150 50  0001 L CNN
F 4 "Film Capacitors MKP , 25 uF , 5% , 500Vdc, pitch 37,5 , 4 pins" H 10050 5050 50  0001 L CNN "Description"
F 5 "38.5" H 10050 4950 50  0001 L CNN "Height"
F 6 "Vishay" H 10050 4850 50  0001 L CNN "Manufacturer_Name"
F 7 "MKP1848C62550JP4" H 10050 4750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "MKP1848C62550JP4" H 10050 4650 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/mkp1848c62550jp4/vishay" H 10050 4550 50  0001 L CNN "Arrow Price/Stock"
F 10 "N/A" H 10050 4450 50  0001 L CNN "Mouser Part Number"
F 11 "https://www.mouser.com/Search/Refine.aspx?Keyword=N%2FA" H 10050 4350 50  0001 L CNN "Mouser Price/Stock"
	1    9400 5150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10200 5050 10200 5800
Wire Wire Line
	9500 4350 9500 4250
Wire Wire Line
	9500 5150 9950 5150
Wire Wire Line
	4000 6050 4350 6050
Wire Wire Line
	8950 4250 9500 4250
Wire Wire Line
	8950 4250 8950 4350
Wire Wire Line
	7350 3000 7350 2800
Wire Wire Line
	8850 2200 8850 2400
Wire Wire Line
	5250 8950 5700 8950
Wire Wire Line
	9400 4350 8950 4350
Connection ~ 8950 4350
Wire Wire Line
	8950 4350 8950 5050
Wire Wire Line
	9400 5150 9500 5150
Connection ~ 9500 5150
Wire Wire Line
	11250 5800 11350 5800
Connection ~ 11250 5800
Connection ~ 11350 5800
Wire Wire Line
	11250 6650 11350 6650
Connection ~ 11250 6650
Connection ~ 11350 6650
$Comp
L power:VPP #PWR015
U 1 1 5F8E6D69
P 8950 4050
F 0 "#PWR015" H 8950 3900 50  0001 C CNN
F 1 "VPP" H 8965 4223 50  0000 C CNN
F 2 "" H 8950 4050 50  0001 C CNN
F 3 "" H 8950 4050 50  0001 C CNN
	1    8950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4250 8950 4100
Connection ~ 8950 4250
Wire Wire Line
	8950 4100 9150 4100
Connection ~ 8950 4100
Wire Wire Line
	8950 4100 8950 4050
$Comp
L power:VPP #PWR012
U 1 1 5F931AC2
P 7650 2750
F 0 "#PWR012" H 7650 2600 50  0001 C CNN
F 1 "VPP" H 7665 2923 50  0000 C CNN
F 2 "" H 7650 2750 50  0001 C CNN
F 3 "" H 7650 2750 50  0001 C CNN
	1    7650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2800 7650 2800
Wire Wire Line
	7650 2800 7650 2750
$Comp
L Device:C C25
U 1 1 5F9B4C41
P 12200 2950
F 0 "C25" H 12315 2996 50  0000 L CNN
F 1 "0.1u" H 12315 2905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 12238 2800 50  0001 C CNN
F 3 "~" H 12200 2950 50  0001 C CNN
	1    12200 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5F9B59E2
P 12200 3650
F 0 "C26" H 12315 3696 50  0000 L CNN
F 1 "0.1u" H 12315 3605 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 12238 3500 50  0001 C CNN
F 3 "~" H 12200 3650 50  0001 C CNN
	1    12200 3650
	0    1    1    0   
$EndComp
$Comp
L Device:C C23
U 1 1 5F9B6453
P 10700 4150
F 0 "C23" H 10815 4196 50  0000 L CNN
F 1 "0.1u" H 10815 4105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 10738 4000 50  0001 C CNN
F 3 "~" H 10700 4150 50  0001 C CNN
	1    10700 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:C C24
U 1 1 5F9B7376
P 11250 3300
F 0 "C24" H 11365 3346 50  0000 L CNN
F 1 "0.1u" H 11365 3255 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 11288 3150 50  0001 C CNN
F 3 "~" H 11250 3300 50  0001 C CNN
	1    11250 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 2800 12200 2800
Connection ~ 11950 2800
$Comp
L power:GND #PWR018
U 1 1 5F9D4FF5
P 12200 3100
F 0 "#PWR018" H 12200 2850 50  0001 C CNN
F 1 "GND" H 12205 2927 50  0000 C CNN
F 2 "" H 12200 3100 50  0001 C CNN
F 3 "" H 12200 3100 50  0001 C CNN
	1    12200 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5F9D525F
P 12450 3550
F 0 "#PWR019" H 12450 3300 50  0001 C CNN
F 1 "GND" H 12455 3377 50  0000 C CNN
F 2 "" H 12450 3550 50  0001 C CNN
F 3 "" H 12450 3550 50  0001 C CNN
	1    12450 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	12350 3650 12450 3650
Wire Wire Line
	12450 3650 12450 3550
Wire Wire Line
	12050 3650 11950 3650
Connection ~ 11950 3650
Wire Wire Line
	10850 3950 10700 3950
Wire Wire Line
	10700 3950 10700 4000
Connection ~ 10850 3950
$Comp
L power:GND #PWR016
U 1 1 5FA29F8A
P 10700 4300
F 0 "#PWR016" H 10700 4050 50  0001 C CNN
F 1 "GND" H 10705 4127 50  0000 C CNN
F 2 "" H 10700 4300 50  0001 C CNN
F 3 "" H 10700 4300 50  0001 C CNN
	1    10700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 3100 10700 3100
Wire Wire Line
	10700 3100 10700 3300
Wire Wire Line
	10700 3300 11100 3300
Connection ~ 10850 3100
$Comp
L power:GND #PWR017
U 1 1 5FA46ACF
P 11400 3300
F 0 "#PWR017" H 11400 3050 50  0001 C CNN
F 1 "GND" V 11405 3172 50  0000 R CNN
F 2 "" H 11400 3300 50  0001 C CNN
F 3 "" H 11400 3300 50  0001 C CNN
	1    11400 3300
	0    -1   -1   0   
$EndComp
NoConn ~ 11950 5250
$Comp
L ACS780LLRTR-100B-T:ACS780LLRTR-100B-T U11
U 1 1 5F644244
P 11350 5150
F 0 "U11" H 11350 5715 50  0000 C CNN
F 1 "ACS780LLRTR-100B-T" H 11350 5624 50  0000 C CNN
F 2 "BuckConverter:XDCR_ACS780LLRTR-100B-T" H 11350 5150 50  0001 L BNN
F 3 "Allegro Microsystems" H 11350 5150 50  0001 L BNN
F 4 "Manufacturer recommendations" H 11350 5150 50  0001 L BNN "Field4"
F 5 "4" H 11350 5150 50  0001 L BNN "Field5"
F 6 "1.6mm" H 11350 5150 50  0001 L BNN "Field6"
	1    11350 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R28
U 1 1 5F83A781
P 8200 4100
F 0 "R28" V 7995 4100 50  0000 C CNN
F 1 "47k" V 8086 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8240 4090 50  0001 C CNN
F 3 "~" H 8200 4100 50  0001 C CNN
	1    8200 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R12
U 1 1 5F6A440D
P 7700 3500
F 0 "R12" V 7495 3500 50  0000 C CNN
F 1 "10" V 7586 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7740 3490 50  0001 C CNN
F 3 "~" H 7700 3500 50  0001 C CNN
	1    7700 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R9
U 1 1 5F5B79A0
P 7450 3850
F 0 "R9" V 7245 3850 50  0000 C CNN
F 1 "10" V 7336 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7490 3840 50  0001 C CNN
F 3 "~" H 7450 3850 50  0001 C CNN
	1    7450 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R5
U 1 1 5F52CD36
P 5700 4250
F 0 "R5" V 5495 4250 50  0000 C CNN
F 1 "47k" V 5586 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5740 4240 50  0001 C CNN
F 3 "~" H 5700 4250 50  0001 C CNN
	1    5700 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 5F5838DA
P 3900 3850
F 0 "R1" V 3695 3850 50  0000 C CNN
F 1 "470" V 3786 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3940 3840 50  0001 C CNN
F 3 "~" H 3900 3850 50  0001 C CNN
	1    3900 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R2
U 1 1 5F6C2BE9
P 3900 4050
F 0 "R2" V 3695 4050 50  0000 C CNN
F 1 "470" V 3786 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3940 4040 50  0001 C CNN
F 3 "~" H 3900 4050 50  0001 C CNN
	1    3900 4050
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R8
U 1 1 5F75B43C
P 6850 5050
F 0 "R8" V 6645 5050 50  0000 C CNN
F 1 "47k" V 6736 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6890 5040 50  0001 C CNN
F 3 "~" H 6850 5050 50  0001 C CNN
	1    6850 5050
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R6
U 1 1 5F75B3B3
P 5800 7250
F 0 "R6" V 5595 7250 50  0000 C CNN
F 1 "47k" V 5686 7250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5840 7240 50  0001 C CNN
F 3 "~" H 5800 7250 50  0001 C CNN
	1    5800 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 5F75B44F
P 4000 6850
F 0 "R3" V 3795 6850 50  0000 C CNN
F 1 "470" V 3886 6850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4040 6840 50  0001 C CNN
F 3 "~" H 4000 6850 50  0001 C CNN
	1    4000 6850
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R4
U 1 1 5F75B4B4
P 4000 7050
F 0 "R4" V 3795 7050 50  0000 C CNN
F 1 "470" V 3886 7050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4040 7040 50  0001 C CNN
F 3 "~" H 4000 7050 50  0001 C CNN
	1    4000 7050
	0    1    1    0   
$EndComp
$EndSCHEMATC
